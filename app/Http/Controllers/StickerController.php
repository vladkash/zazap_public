<?php

namespace App\Http\Controllers;

use App\Sticker;
use Illuminate\Http\Request;

class StickerController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    public function addSticker(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:stickers',
            'price'=>'required|numeric',
            'sticker'=>'required|image'
        ]);
        $data = $request->all();
        $guessExtension = $request->file('sticker')->guessExtension();
        $sticker_id = Sticker::max('id') + 1;
        $request->file('sticker')->storeAs('public/stickers', 'sticker'.$sticker_id.'.'.$guessExtension );
        Sticker::create([
            'name' => $data['name'],
            'price'=>$data['price'],
            'sticker'=>'sticker'.$sticker_id.'.'.$guessExtension
        ]);
        return redirect('/stickers');
    }

    public function editSticker($id, Request $request)
    {
        $sticker = Sticker::find($id);
        $data = $request->all();

        if ($sticker->name != $data['name']) {
            $request->validate([
                'name'=>'unique:stickers',
            ]);
            $sticker->name = $data['name'];
        }
        if ($sticker->price != $data['price']) {
            $request->validate([
                'price'=>'numeric',
            ]);
            $sticker->price = $data['price'];
        }
        if(!empty($request->file('sticker'))){
            $request->validate([
                'sticker'=>'image'
            ]);
            if (file_exists(public_path('storage/stickers/'.$sticker->sticker))) {
                unlink(public_path('storage/stickers/'.$sticker->sticker));
            }
            $guessExtension = $request->file('sticker')->guessExtension();
            $request->file('sticker')->storeAs('public/stickers', 'sticker'.$sticker->id.'.'.$guessExtension );
            $sticker->sticker = 'sticker'.$sticker->id.'.'.$guessExtension;
        }
        $sticker->save();
        return redirect('/stickers');
    }

    public function stickerDelete($id)
    {
        $sticker = Sticker::find($id);
        $sticker->delete();
        return redirect('/stickers');
    }
}
