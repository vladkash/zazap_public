<?php

namespace App\Http\Controllers\API;

use App\Chat;
use App\Sticker;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class StickerController extends Controller
{
    private $successStatus = 200;

    public function getStickers()
    {
        $stickers = Sticker::all();
        $response = new ApiResponse($stickers, null, null);
        return response()->json($response, $this->successStatus);
    }

    public function addStickerMessage(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'chat_id'=>'required|exists:chats,id',
            'sticker_id'=>'required|exists:stickers,id'
        ]);

        if ($validator->fails()) {
            $response = new ApiResponse(null, $validator->errors(), 'Validation errors');
            return response()->json($response, 400);
        }
        $user = Auth::user();
        $data = $request->all();
        $vip = $user->vip()->latest()->first();
        if (!$vip || strtotime($vip->end_at) <= strtotime(date('Y-m-d H:i:s'))) {
            $response = new ApiResponse(null, null, ["You haven't vip status!"]);
            return response()->json($response, 400);
        }
        $chat = Chat::find($data['chat_id']);
        if(!$chat && ($chat->user1 == $user->id || $chat->user2 == $user->id)){
            $response = new ApiResponse(null, null, ["Chat not found!"]);
            return response()->json($response, 400);
        }
        if ($chat->user1 == $user->id) {
            $friend_id = $chat->user2;
        }else{
            $friend_id = $chat->user1;
        }
        $message = $chat->messages()->create([
            'text' => null,
            'sender_id' => $user->id,
            'recipient_id'=> $friend_id,
        ]);
        $sticker = $message->sticker()->create([
            'sticker_id' => $data['sticker_id']
        ]);
        $sticker = Sticker::find($sticker->sticker_id);
        $message->sticker = $sticker;
        $response = new ApiResponse($message, null, null);
        return response()->json($response, $this->successStatus);
    }
}
