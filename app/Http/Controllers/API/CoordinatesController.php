<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Placemark;

class CoordinatesController extends Controller
{
    public $successStatus = 200;

    public function getCoordinates(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'latitude'=>'required',
            'longitude'=>'required',
            'radius'=>'required'
        ]);

        if ($validator->fails()) {
            $response = new ApiResponse(null, $validator->errors(), 'Validation errors');
            return response()->json($response, 400);
        }
        $data = $request->all();
        $current_user = Auth::user();
        $coordinates = $current_user->coordinates;
        $coordinates->latitude = $data['latitude'];
        $coordinates->longitude = $data['longitude'];
        $coordinates->save();
        $users = User::all();
        $near_coordinates = [];
        foreach ($users as $user)
        {
            if ($user->id != $current_user->id && $user->coordinates) {
                $current_coordinates = $user->coordinates;
                $distance = $this->getDistance($coordinates->latitude,$coordinates->longitude, $current_coordinates->latitude,$current_coordinates->longitude);
                if($distance <= $data['radius'])
                {
                    $photo = $user->photo()->where('is_avatar', 1)->where('moderated', 1)->first();
                    array_push($near_coordinates,[
                        'name'=>$user->name,
                        'latitude'=>$current_coordinates->latitude,
                        'longitude'=>$current_coordinates->longitude,
                        'photo' => $photo ? url('/storage/avatars/'.$photo->name_photo): url('/assets/img/no_photo.jpg')
                    ]);
                }
            }
        }
        $response = new ApiResponse($near_coordinates, null, null);
        return response()->json($response, $this->successStatus);
    }

    public function getMarkers(){
        $markers = Placemark::where('status','Активна')->get();
        foreach ($markers as $marker){
            if ($marker->image) {
                $marker->image = url('/storage/placemarkers').'/'.$marker->image;
            }
        }
        $response = new ApiResponse($markers, null, null);
        return response()->json($response, $this->successStatus);
    }

    public  function getDistance( $latitude1, $longitude1, $latitude2, $longitude2 ) {
        $earth_radius = 6371;

        $dLat = deg2rad( $latitude2 - $latitude1 );
        $dLon = deg2rad( $longitude2 - $longitude1 );

        $a = sin($dLat/2) * sin($dLat/2) + cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * sin($dLon/2) * sin($dLon/2);
        $c = 2 * asin(sqrt($a));
        $d = $earth_radius * $c;

        return $d;
    }

    public function getGuestCoordinates(Request $request)
    {
        $data = $request->all();
        if (!array_key_exists('latitude', $data)) {
            $data['latitude'] = 55.753512;
        }
        if (!array_key_exists('longitude', $data)) {
            $data['longitude'] = 37.622930;
        }
        if (!array_key_exists('radius', $data)) {
            $data['radius'] = 7;
        }
        $users = User::all();
        $near_coordinates = [];
        foreach ($users as $user)
        {
            if ($user->coordinates) {
                $current_coordinates = $user->coordinates;
                $distance = $this->getDistance($data['latitude'], $data['longitude'], $current_coordinates->latitude,$current_coordinates->longitude);
                if($distance <= $data['radius'])
                {
                    $photo = $user->photo()->where('is_avatar', 1)->where('moderated', 1)->first();
                    array_push($near_coordinates,[
                        'name'=>$user->name,
                        'latitude'=>$current_coordinates->latitude,
                        'longitude'=>$current_coordinates->longitude,
                        'photo' => $photo ? url('/storage/avatars/'.$photo->name_photo): url('/assets/img/no_photo.jpg')
                    ]);
                }
            }
        }
        $response = new ApiResponse($near_coordinates, null, null);
        return response()->json($response, $this->successStatus);
    }

}
