<?php

namespace App\Http\Controllers\API;

use App\Photo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Validator;

class PhotoController extends Controller
{
    public $successStatus = 200;

    public function addPhoto(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'photo'=>'required|image',
        ]);
        if ($validator->fails()) {
            $response = new ApiResponse(null, $validator->errors(), 'Validation errors');
            return response()->json($response, 400);
        }
        $user = Auth::user();
        $guessExtension = $request->file('photo')->guessExtension();
        $photo_id = Photo::max('id') + 1;
        $request->file('photo')->storeAs('public/avatars', 'user'.$photo_id.'avatar'.'.'.$guessExtension );
        $photo = $user->photo()->create([
            'name_photo'=>'user'.$photo_id.'avatar'.'.'.$guessExtension,
            'moderated'=> 1
        ]);
        $photo->name_photo = url('/storage/avatars/'.$photo->name_photo);
        $response = new ApiResponse($photo, null, null);
        return response()->json($response, $this->successStatus);
    }

    public function deletePhoto(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'photo_id'=>'required',
        ]);

        if ($validator->fails()) {
            $response = new ApiResponse(null, $validator->errors(), 'Validation errors');
            return response()->json($response, 400);
        }
        $data = $request->all();
        $user = Auth::user();
        $photo = $user->photo()->find($data['photo_id']);
        if ($photo) {
            $path = public_path('/storage/avatars/' . $photo->name_photo);
            if (file_exists($path)) {
                unlink($path);
            }
            $check_avatar = $photo->is_avatar;
            $photo->forceDelete();
            if ($check_avatar && $user->photo) {
                $user->photo[0]->is_avatar = 1;
                $user->photo[0]->save();
            }
        }
        $response = new ApiResponse(['All is right!'], null, null);
        return response()->json($response, $this->successStatus);
    }

    public function changeAvatar(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'avatar_id'=>'required|exists:photos,id'
        ]);

        if ($validator->fails()) {
            $response = new ApiResponse(null, $validator->errors(), 'Validation errors');
            return response()->json($response, 400);
        }
        $data = $request->all();
        $user = Auth::user();
        $current_avatar = $user->photo()->where('is_avatar',1)->first();
        if ($current_avatar) {
            $current_avatar->is_avatar = 0;
            $current_avatar->save();
        }
        $new_avatar = $user->photo()->find($data['avatar_id']);
        if ($new_avatar) {
            $new_avatar->is_avatar = 1;
            $new_avatar->save();
        }else{
            $response = new ApiResponse(null, null, ['New avatar not found!']);
            return response()->json($response, $this->successStatus);
        }
        $response = new ApiResponse(['All is right!'], null, null);
        return response()->json($response, $this->successStatus);
    }

    public function report(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'photo_id'=>'required|exists:photos,id'
        ]);

        if ($validator->fails()) {
            $response = new ApiResponse(null, $validator->errors(), 'Validation errors');
            return response()->json($response, 400);
        }
        $user = Auth::user();
        $exsit_report = $user->reports()->where('photo_id', $request->input('photo_id'))->first();
        if (!$exsit_report) {
            $report = $user->reports()->create([
                'photo_id'=>$request->input('photo_id')
            ]);
            $response = new ApiResponse($report, null, null);
            return response()->json($response, $this->successStatus);
        } else {
            $response = new ApiResponse(null, null, ['Report already exists!']);
            return response()->json($response, 400);
        }
    }

}
