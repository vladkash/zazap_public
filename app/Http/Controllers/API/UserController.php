<?php

namespace App\Http\Controllers\API;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public $successStatus = 200;

    public function getUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id'=>'required'
        ]);

        if ($validator->fails()) {
            $response = new ApiResponse(null, $validator->errors(), 'Validation errors');
            return response()->json($response, 400);
        }
        $data = $request->all();
        $user = User::where('id',$data['user_id'])->where('is_admin', 0)->first();
        if ($user) {
            $photos = $user->photo()->where('moderated', 1)->get();
            foreach ($photos as $photo) {
                $photo->name_photo = url('/storage/avatars/'.$photo->name_photo);
                $photo->likes = $photo->likes()->count();
            }
            $user->photo = $photos;
            $user->count_subscribes = $user->subscribes()->count();
            $user->count_subscribers = $user->subscribers()->count();
            $response = new ApiResponse($user, null, null);
            return response()->json($response, $this->successStatus);
        } else {
            $response = new ApiResponse(null, null, "Пользователь не найден!");
            return response()->json($response, $this->successStatus);
        }

    }

    public function getCurrentUser()
    {
        $user = Auth::user();
        $user->count_subscribes = $user->subscribes()->count();
        $user->count_subscribers = $user->subscribers()->count();
        $photos = $user->photo()->where('moderated', 1)->get();
        foreach ($photos as $photo) {
            $photo->name_photo = url('/storage/avatars/'.$photo->name_photo);
            $photo->likes = $photo->likes()->count();
        }
        $user->photo = $photos;
        $user->vip = $user->vip()->whereDate('end_at', '>', date('Y-m-d H:i:s'))->first();
        $response = new ApiResponse($user, null, null);
        return response()->json($response, $this->successStatus);
    }

    public function updateProfile(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'=>'unique:users',
            'sex'=>'exists:users',
            'email'=>'unique:users',
            'phone'=>'unique:users',
            'birthdate'=>'date',
            'firstname'=>'string|nullable',
            'surname'=>'string|nullable',
            'city'=>'string|nullable',
            'description'=>'string|nullable'
        ]);

        if ($validator->fails()) {
            $response = new ApiResponse(null, $validator->errors(), 'Validation errors');
            return response()->json($response, 400);
        }
        $user = Auth::user();
        //update only available fields
        $user = User::where('id',$user->id)->update($request->only('name','sex','email','phone','birthdate','firstname','surname','city','description'));
        $response = new ApiResponse($user, null, null);
        return response()->json($response, $this->successStatus);
    }

    public function heartbeat(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'latitude'=>'required',
            'longitude'=>'required'

        ]);

        if ($validator->fails()) {
            $response = new ApiResponse(null, $validator->errors(), 'Validation errors');
            return response()->json($response, 400);
        }
        $user = Auth::user();
        User::where('id',$user->id)->update(['online_date'=>date('Y-m-d H:i:s')]);
        $user->coordinates()->update($request->only('latitude','longitude'));
        $response = new ApiResponse(['Success'], null, null);
        return response()->json($response, $this->successStatus);
    }

}
