<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class LikeController extends Controller
{
    public function likePhoto(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'photo_id'=>'required|exists:photos,id'
        ]);

        if ($validator->fails()) {
            $response = new ApiResponse(null, $validator->errors(), 'Validation errors');
            return response()->json($response, 400);
        }
        $user = Auth::user();
        $exist_like = $user->likes()->where('photo_id', $request->input('photo_id'))->first();
        if ($exist_like) {
            $response = new ApiResponse(null, null, ['Photo already liked!']);
            return response()->json($response, 400);
        }
        $like = $user->likes()->create([
            'photo_id' => $request->input('photo_id')
        ]);
        $response = new ApiResponse($like, null, null);
        return response()->json($response, 200);
    }

    public function deleteLike(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'photo_id'=>'required|exists:photos,id'
        ]);

        if ($validator->fails()) {
            $response = new ApiResponse(null, $validator->errors(), 'Validation errors');
            return response()->json($response, 400);
        }
        $user = Auth::user();
        $like = $user->likes()->where('photo_id', $request->input('photo_id'))->first();
        if (!$like) {
            $response = new ApiResponse(null, null, ['Like does not exist!']);
            return response()->json($response, 400);
        }
        $like->delete();
        $response = new ApiResponse(['Like has deleted!'], null, null);
        return response()->json($response, 200);
    }
}
