<?php

namespace App\Http\Controllers\API;

use App\Chat;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class NotificationController extends Controller
{
    private $rows = [];
    private $set_readed = false;

    public function getNotifications()
    {
        $user = Auth::user();
//        $chats = Chat::where('user1', $user->id)->orWhere('user2', $user->id)->get();
        $notifications = 0;
        /*foreach ($chats as $chat) {
            $new_messages = $chat->messages()->where('recipient_id', $user->id)->where('readed', 0)->count();
            $notifications += $new_messages;
        }*/
        $new_compliments = $user->compliments()->where('viewed', 0)->count();
        $notifications += $new_compliments;
        foreach ($user->photo as $photo) {
            $likes = $photo->likes()->where('viewed', 0)->count();
            $notifications += $likes;
        }
        $new_subscribers = $user->subscribers()->where('viewed', 0)->count();
        $notifications += $new_subscribers;
        $response = new ApiResponse($notifications, null, null);
        return response()->json($response, 200);
    }

    public function getNotificationsDetails()
    {
        $user = Auth::user();
        $new_compliments = $user->compliments()->where('viewed', 0)->get();
        $this->rows[] = $new_compliments;
        foreach ($new_compliments as $compliment) {
            $compliment->friend = User::find($compliment->friend_id);
            $compliment->friend->photo = $compliment->friend->photo()->where('is_avatar',1)->where('moderated', 1)->first();
            if ($compliment->friend->photo) {
                $compliment->friend->photo->name_photo = url('storage/avatars/'.$compliment->friend->photo->name_photo);
            }
        }
        $new_likes = [];
        foreach ($user->photo as $photo) {
            $likes = $photo->likes()->where('viewed', 0)->get();
            $this->rows[] = $new_likes;
            foreach ($likes as $like) {
                $user = $like->user;
                if ($user) {
                    $photo = $user->photo()->where('is_avatar',1)->where('moderated', 1)->first();
                    $photo->name_photo = url('storage/avatars/'.$photo->name_photo);
                    $user->photo = $photo;
                    $like->friend = $user;
                }
            }
            if (!empty($likes)) {
                $new_likes[] =  $likes;
            }
        }
        $new_subscribers = $user->subscribers()->where('viewed', 0)->get();
        $this->rows[] = $new_subscribers;
        foreach ($new_subscribers as $new_subscriber) {
            $user = $new_subscriber->user;
            $photo = $user->photo()->where('is_avatar',1)->where('moderated', 1)->first();
            if ($photo) {
                $photo->name_photo = url('storage/avatars/'.$photo->name_photo);
                $user->photo = $photo;
            }
            $new_subscriber->friend = $user;
        }
        $this->set_readed = true;
        $response = new ApiResponse(compact('new_compliments','new_likes'), null, null);
        return response()->json($response, 200);
    }

    public function __destruct()
    {
        if ($this->set_readed) {
            foreach ($this->rows as $types) {
                foreach ($types as $row) {
                    $row->readed = 1;
                    $row->save();
                }
            }
        }
    }

}
