<?php

namespace App\Http\Controllers\API;


class ApiResponse
{
    public $error;
    public $message;
    public $datapayload;

    public function __construct($datapayload, $error, $message)
    {
        $this->datapayload = $datapayload;
        $this->error = $error;
        $this->message = $message;
    }
}