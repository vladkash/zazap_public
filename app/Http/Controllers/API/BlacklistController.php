<?php

namespace App\Http\Controllers\API;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class BlacklistController extends Controller
{
    private  $successStatus = 200;

    public function getBlacklist()
    {
        $user = Auth::user();
        $blacklist = $user->blacklist;
        $result = [];
        foreach ($blacklist as $blackuser) {
            $user = User::find($blackuser->block_id);
            if ($user) {
                $user->photo = $user->photo()->where('moderated', 1)->where('is_avatar', 1);
                $result[] = $user;
            }
        }
        $response = new ApiResponse($result, null, null);
        return response()->json($response, $this->successStatus);
    }

    public function blockUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'block_id'=>'required'
        ]);

        if ($validator->fails()) {
            $response = new ApiResponse(null, $validator->errors(), 'Validation errors');
            return response()->json($response, 400);
        }
        $data = $request->all();
        $user = Auth::user();
        $user->blacklist()->create([
            'block_id'=>$data['block_id']
        ]);
        $response = new ApiResponse(['User has blocked!'], null, null);
        return response()->json($response, $this->successStatus);
    }

    public function unblockUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'unblock_id'=>'required'
        ]);

        if ($validator->fails()) {
            $response = new ApiResponse(null, $validator->errors(), 'Validation errors');
            return response()->json($response, 400);
        }
        $data = $request->all();
        $user = Auth::user();
        $user->blacklist()->where('block_id', $data['unblock_id'])->delete();
        $response = new ApiResponse(['User has unblocked!'], null, null);
        return response()->json($response, $this->successStatus);
    }
}
