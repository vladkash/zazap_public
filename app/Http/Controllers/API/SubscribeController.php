<?php

namespace App\Http\Controllers\API;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class SubscribeController extends Controller
{
    public $successStatus = 200;

    public function subscribe(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'subscribe_id'=>'required|exists:users,id'
        ]);

        if ($validator->fails()) {
            $response = new ApiResponse(null, $validator->errors(), 'Validation errors');
            return response()->json($response, 400);
        }
        $data = $request->all();
        $user = Auth::user();
        if ($data['subscribe_id'] == $user->id) {
            $response = new ApiResponse(null, null, 'Selfsubscribe!');
            return response()->json($response, $this->successStatus);
        }
        $subscribe = $user->subscribes()->firstOrCreate([
            'subscribe_id'=>$data['subscribe_id']
        ]);
        $response = new ApiResponse($subscribe, null, null);
        return response()->json($response, $this->successStatus);
    }

    public function unsubscribe(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'unsubscribe_id'=>'required|exists:users,id'
        ]);

        if ($validator->fails()) {
            $response = new ApiResponse(null, $validator->errors(), 'Validation errors');
            return response()->json($response, 400);
        }
        $data = $request->all();
        $user = Auth::user();
        $subscribe = $user->subscribes()->where('subscribe_id',$data['unsubscribe_id'])->first();
        if (!$subscribe) {
            $response = new ApiResponse(null, null, ['Subscribe not found!']);
            return response()->json($response, $this->successStatus);
        }
        $subscribe->delete();
        $response = new ApiResponse(['Subscribe has deleted'], null, null);
        return response()->json($response, $this->successStatus);
    }

    public function getSubscribes()
    {
        $user = Auth::user();
        $subscribes = $user->subscribes;
        $result = [];
        foreach ($subscribes as $subscribe) {
            $user = User::find($subscribe->subscribe_id);
            $photo = $user->photo()->where('moderated', 1)->where('is_avatar', 1)->first();
            $user->photo = $photo ? url('/storage/avatars/'.$photo->name_photo):null;
            $result[] = $user;
        }
        $response = new ApiResponse($result, null, null);
        return response()->json($response, $this->successStatus);
    }
}
