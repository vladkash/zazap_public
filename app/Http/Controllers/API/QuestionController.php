<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Question;
use App\Events\Newprivatechat;
use Illuminate\Support\Facades\Validator;

class QuestionController extends Controller
{
    public $successStatus = 200;
    public $setReadedMessages = false;
    public $messages;

    public function getQuestions()
    {
        $user = Auth::user();
        $questions = $user->questions;
        foreach ($questions as $question) {
            $message = $question->messages()->latest()->first();
            $question->last_message = $message;
        }
        $response = new ApiResponse($questions, null, null);
        return response()->json($response, $this->successStatus);
    }

    public function getQuestionMessages(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'question_id'=>'required'
        ]);

        if ($validator->fails()) {
            $response = new ApiResponse(null, $validator->errors(), 'Validation errors');
            return response()->json($response, 400);
        }
        $data = $request->all();
        $user = Auth::user();
        $question = Question::find($data['question_id']);
        if ($question &&  $question->user_id == $user->id) {
            $messages = $question->messages;
            $this->setReadedMessages = true;
            $this->messages = $messages;
            $response = new ApiResponse($messages, null, null);
            return response()->json($response, $this->successStatus);
        }else{
            $response = new ApiResponse(null, null, ['Access denied!']);
            return response()->json($response, $this->successStatus);
        }
    }

    public function addQuestionMessage(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'message'=>'required'
        ]);

        if ($validator->fails()) {
            $response = new ApiResponse(null, $validator->errors(), 'Validation errors');
            return response()->json($response, 400);
        }
        $data = $request->all();
        $user = Auth::user();
        if (array_key_exists('question_id',$data)) {
            $question = Question::find($data['question_id']);
            if ($question && $question->user_id == $user->id) {
                $message = $question->messages()->create([
                    'is_admin'=>0,
                    'readed'=> 0,
                    'message'=>$data['message']
                ]);
                Newprivatechat::dispatch($message);
                $response = new ApiResponse($message, null, null);
                return response()->json($response, $this->successStatus);
            }else{
                $response = new ApiResponse(null, null, ['Access denied!']);
                return response()->json($response, $this->successStatus);
            }
        }else{
            $question = $user->questions()->create([
                'status'=>'Новый'
            ]);
            $message = $question->messages()->create([
                'is_admin'=>0,
                'readed'=>0,
                'message'=>$data['message']
            ]);
            $response = new ApiResponse(['question'=>$question, 'message'=>$message], null, null);
            return response()->json($response, $this->successStatus);
        }
    }

    public function __destruct()
    {
        if ($this->setReadedMessages) {
            foreach ($this->messages as $message){
                if(!$message->readed && $message->is_admin){
                    $message->readed = 1;
                    $message->save();
                }
            }
        }
    }

}
