<?php

namespace App\Http\Controllers\API;

use App\Chat;
use App\MessageContent;
use App\Sticker;
use App\User;
use App\Events\Newprivatechat;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class MessageController extends Controller
{
    public $successStatus = 200;
    public $setReadedMessages = false;
    public $messages;
    public $recipient_id;

     public function getMessages(Request $request)
     {
         $validator = Validator::make($request->all(), [
             'chat_id'=>'required'
         ]);

         if ($validator->fails()) {
             $response = new ApiResponse(null, $validator->errors(), 'Validation errors');
             return response()->json($response, 401);
         }
         $user = Auth::user();
         $data = $request->all();
         $chat = Chat::find($data['chat_id']);
         if (!$chat) {
             $response = new ApiResponse(null, null, ['Chat not found!']);
             return response()->json($response, 401);
         }
         $blacklist = $this->checkBlackList($user, $chat);
         if($chat && empty($blacklist)){
             if ($chat->user1 == $user->id || $chat->user2 == $user->id) {
                 $messages = $chat->messages()->orderBy('id','desc')->paginate(15);
                 foreach ($messages as $message) {
                     $content = $message->content;
                     if ($content) {
                         $message->content = $content;
                     }
                     $message_sticker = $message->sticker;
                     if ($message_sticker) {
                         $sticker = Sticker::find($message_sticker->sticker_id);
                         unset($message->sticker);
                         $message->sticker = $sticker;
                     }
                 }
                 $response = new ApiResponse( $messages, null, null);
                 $this->setReadedMessages = true;
                 $this->messages = $messages;
                 $this->recipient_id = $user->id;
                 return response()->json($response, $this->successStatus);
             }else{
                 $response = new ApiResponse(null, null, ['Access error!']);
                 return response()->json($response, $this->successStatus);
             }
         }else{
             $response = new ApiResponse(null, null, ['Chat not found!']);
             return response()->json($response, $this->successStatus);
         }
     }

     public function addMessage(Request $request)
     {
         if (empty($request->file('content'))) {
             $validator = Validator::make($request->all(), [
                 'message'=>'required'
             ]);
             if ($validator->fails()) {
                 $response = new ApiResponse(null, $validator->errors(), 'Validation errors');
                 return response()->json($response, 401);
             }
         }
         $allowed_extensions = ['jpg','png','mov','mp4','m4v','gif','jpeg'];

         $data = $request->all();
         $user = Auth::user();
         if (array_key_exists('chat_id',$data)) {
             return $this->sendChatMessage($request, $user, $allowed_extensions);
         }elseif(array_key_exists('friend_id',$data)){
             return $this->sendFriendMessage($request, $user, $allowed_extensions);
         }else{
             $response = new ApiResponse(null, null, ['Parameters not found!']);
             return response()->json($response, $this->successStatus);
         }
     }

    private function sendChatMessage(Request $request, $user, $allowed_extensions)
    {
        $data = $request->all();
        if (!array_key_exists('message', $data)) {
            $data['message'] = null;
        }
        $chat = Chat::find($data['chat_id']);
        if($chat && ($chat->user1 == $user->id || $chat->user2 == $user->id)){
            if ($chat->user1 == $user->id) {
                $friend_id = $chat->user2;
            }else{
                $friend_id = $chat->user1;
            }
            $friend = User::find($friend_id);
            $blacklist = $this->checkBlackList($friend, $chat);
            if (empty($blacklist)) {
                $message = $chat->messages()->create([
                    'sender_id'=>$user->id,
                    'recipient_id'=>$friend_id,
                    'text'=>$data['message']
                ]);
                $message = $this->addContent($request, $allowed_extensions, $message);
                $response = new ApiResponse($message, null, null);
                return response()->json($response, $this->successStatus);
            }else{
                $response = new ApiResponse(null, null, ['You are in blacklist!']);
                return response()->json($response, $this->successStatus);
            }
        }else{
            $response = new ApiResponse(null, null, ['Chat not found!']);
            return response()->json($response, $this->successStatus);
        }
    }

    private function sendFriendMessage(Request $request, $user, $allowed_extensions)
    {
        $data = $request->all();
        if (!array_key_exists('message', $data)) {
            $data['message'] = null;
        }
        $check_exists = Chat::where(function ($query) use($user,$data) {
            $query->where('user1', $user->id)
                ->where('user2',$data['friend_id']);
        })->orWhere(function ($query) use($user,$data) {
            $query->where('user2', $user->id)
                ->where('user1',$data['friend_id']);
        })->first();
        if ($check_exists) {
            $response = new ApiResponse(null, null, ['Chat already exists!']);
            return response()->json($response, $this->successStatus);
        }
        $chat = Chat::create([
            'user1'=>$user->id,
            'user2'=>$data['friend_id']
        ]);
        $message = $chat->messages()->create([
            'sender_id'=>$user->id,
            'recipient_id'=>$data['friend_id'],
            'text'=>$data['message']
        ]);
        $message = $this->addContent($request, $allowed_extensions, $message);
        $response = new ApiResponse($message, null, null);
        return response()->json($response, $this->successStatus);
    }

    private function addContent(Request $request,$allowed_extensions,$message)
    {
        // check & action with additional content
        if (!empty($request->file('content'))) {
            $guessExtension = $request->file('content')->guessExtension();
            if (in_array(''.$guessExtension, $allowed_extensions)) {
                $content_id = MessageContent::max('id') + 1;
                $request->file('content')->storeAs('public/contents', 'content' . $content_id . '.' . $guessExtension);
                $content = $message->content()->create([
                    'content' => 'content' . $content_id . '.' . $guessExtension
                ]);
                $message->content = $content;
            }else{
                $message->content = 'Not allowed extension: '.$guessExtension;
            }
        }else{
            $message->content = null;
        }
        return $message;
    }
    public function deleteMessage(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'chat_id'=>'required',
            'message_id'=>'required'
        ]);

        if ($validator->fails()) {
            $response = new ApiResponse(null, $validator->errors(), 'Validation errors');
            return response()->json($response, 401);
        }
        $user = Auth::user();
        $data = $request->all();
        $chat = Chat::find($data['chat_id']);
        $message = $chat->messages()->where('id', $data['message_id'])->first();
        if ($message->sender_id == $user->id) {
            $message->delete();
            $response = new ApiResponse(['Message has deleted!'], null, null);
            return response()->json($response, $this->successStatus);
        }else{
            $response = new ApiResponse(null, null, ['Access errors!']);
            return response()->json($response, $this->successStatus);
        }
    }

    public function checkNewMessages()
    {
        $user = Auth::user();
        $chats = Chat::where('user1', $user->id)->orWhere('user2', $user->id)->get();
        foreach ($chats as $chat) {
            $last_message = $chat->messages()->latest()->first();
            if ($last_message->recipient_id == $user->id && $last_message->readed == 0) {
                $chat->has_new_messages = true;
            }else{
                $chat->has_new_messages = false;
            }
            $chat->last_message = $last_message;
        }
        $response = new ApiResponse($chats, null, null);
        return response()->json($response, $this->successStatus);
    }

    public function __destruct()
    {
        if ($this->setReadedMessages) {
            foreach ($this->messages as $message){
                if(!$message->readed && $message->recipient_id == $this->recipient_id){
                    $message->readed = 1;
                    $message->save();
                }
            }
        }
    }

    private function checkBlackList($user, $chat){
        if ($chat->user1 == $user->id) {
            $friend_id = $chat->user2;
        }else{
            $friend_id = $chat->user1;
        }
         $blacklist = $user->blacklist()->where('block_id', $friend_id)->first();
        return $blacklist;
    }
}
