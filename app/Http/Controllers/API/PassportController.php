<?php

namespace App\Http\Controllers\API;

use App\Photo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;

use App\Placemark;

class PassportController extends Controller
{
    public $successStatus = 200;


    public function login(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'password'=>'required',
            'latitude'=>'required',
            'longitude'=>'required'
        ]);

        if ($validator->fails()) {
            $response = new ApiResponse(null, $validator->errors(), 'Validation errors');
            return response()->json($response, 400);
        }
        if(Auth::attempt(['name' => request('name'), 'password' => request('password')])){
            $user = Auth::user();
            $user->lastvisit = date('Y-m-d');
            $user->save();
            $success['token'] =  $user->createToken('MyApp')->accessToken;
            $success['id'] =  $user->id;
            $success['name'] =  $user->name;
            $success['email'] =  $user->email;
            $success['phone'] =  $user->phone;
            $success['sex'] =  $user->sex;
            $success['birthdate'] =  $user->birthdate;
            $success['lastvisit'] =  $user->lastvisit;
            $photos = $user->photo;
            foreach ($photos as $photo) {
                $photo->name_photo =  url('/storage/avatars/'.$photo->name_photo);
            }
            $success['photo'] =  $photos;
            $user->coordinates()->update($request->only('latitude, longitude'));
            $response = new ApiResponse($success, null, null);
            return response()->json($response, $this->successStatus);
        }
        else{
            $response = new ApiResponse(null,null,'Unauthorised');
            return response()->json($response, 401);
        }
    }

    public function checkName(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:users'
        ]);
        if ($validator->fails()) {
            $response = new ApiResponse(null, $validator->errors(), 'Validation errors');
            return response()->json($response, 400);
        }
        $response = new ApiResponse(['All is right!'], null, null);
        return response()->json($response, $this->successStatus);
    }
    public function checkEmail(Request $request){
        $validator = Validator::make($request->all(), [
            'email' => 'required|unique:users'
        ]);
        if ($validator->fails()) {
            $response = new ApiResponse(null, $validator->errors(), 'Validation errors');
            return response()->json($response, 400);
        }
        $response = new ApiResponse(['All is right!'], null, null);
        return response()->json($response, $this->successStatus);
    }
    public function checkPhone(Request $request){
        $validator = Validator::make($request->all(), [
            'phone' => 'required|unique:users'
        ]);
        if ($validator->fails()) {
            $response = new ApiResponse(null, $validator->errors(), 'Validation errors');
            return response()->json($response, 400);
        }
        $response = new ApiResponse(['All is right!'], null, null);
        return response()->json($response, $this->successStatus);
    }
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:users',
            'email' => 'required|email|unique:users',
            'phone' => 'unique:users',
            'password' => 'required',
            'sex'=>'required',
            'birthdate'=>'date',
            'c_password' => 'required|same:password',
            'latitude'=>'required',
            'longitude'=>'required'
        ]);

        if ($validator->fails()) {
            $response = new ApiResponse(null, $validator->errors(), 'Validation errors');
            return response()->json($response, 400);
        }
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create([
            'name'=>$input['name'],
            'email'=>$input['email'],
            'sex'=>$input['sex'],
            'is_admin'=>0,
            'birthdate'=> array_key_exists('birthdate',$input)? date('Y-m-d', strtotime( $input['birthdate'])): null,
            'lastvisit'=>date('Y-m-d'),
            'password'=>$input['password']
        ]);
        $user->coordinates()->create($request->only('latitude,longitude'));
        $success['token'] =  $user->createToken('MyApp')->accessToken;
        $success['id'] =  $user->id;
        $success['name'] =  $user->name;
        $success['email'] =  $user->email;
        $success['phone'] =  $user->phone;
        $success['sex'] =  $user->sex;
        $success['birthdate'] =  $user->birthdate;
        $success['lastvisit'] =  $user->lastvisit;

        $response = new ApiResponse($success, null, null);
        return response()->json($response, $this->successStatus);
    }

    public function sendMessage(Request $request){
        $validator = Validator::make($request->all(), [
            'phone'=>'required',
        ]);

        if ($validator->fails()) {
            $response = new ApiResponse(null, $validator->errors(), 'Validation errors');
            return response()->json($response, 400);
        }
        $data = $request->all();
        $secret = mt_rand(0,9).mt_rand(0,9).mt_rand(0,9).mt_rand(0,9);
        $message = 'Код для подтверждения телефона: '.$secret;
        if( $curl = curl_init() ) {
            curl_setopt($curl, CURLOPT_URL, 'https://smsc.ru/sys/send.php?login='.env('SMSC_LOGIN').'&psw='.env('SMSC_PASSWORD').'&sender=Zazap&charset=utf-8&phones='.$data['phone'].'&mes='.$message);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
            $result = curl_exec($curl);
            curl_close($curl);
            $response = new ApiResponse(['secret'=>$secret,'result'=>$result], null, null);
            return response()->json($response, $this->successStatus);
        }
        $response = new ApiResponse(['Sending error!'], null, null);
        return response()->json($response, $this->successStatus);
    }
}
