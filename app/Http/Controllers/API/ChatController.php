<?php

namespace App\Http\Controllers\API;

use App\BlackList;
use App\Chat;
use App\Sticker;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ChatController extends Controller
{
    public $successStatus = 200;

    public function getChats()
    {
        $user = Auth::user();
        $chats = Chat::where('user1',$user->id)->orWhere('user2',$user->id)->get();
        $result = [];
        foreach ($chats as $chat) {
            if ($chat->user1 == $user->id) {
                $friend_id = $chat->user2;
            }else{
                $friend_id = $chat->user1;
            }
            $black_list = $user->blacklist()->where('block_id',$friend_id)->first();
            if (empty($black_list)) {
                $last_message = $chat->messages()->latest()->first();
                if ($last_message) {
                    $content = $last_message->content;
                    if ($content) {
                        $content->content = url('storage/contens/' . $content->content);
                    }
                    $message_sticker = $last_message->sticker;
                    if ($message_sticker) {
                        $sticker = Sticker::find($message_sticker->sticker_id);
                        unset($last_message->sticker);
                        $last_message->sticker = $sticker;
                    }
                }
                $friend = User::find($friend_id);
                $friend_photo = $friend->photo()->where('is_avatar',1)->where('moderated', 1)->first();
                if ($friend_photo) {
                    $friend_photo->name_photo = url('storage/avatars/'.$friend_photo->name_photo);
                }
                $result[] = [
                    'friend' => $friend,
                    'friend_photo'=>$friend_photo ? $friend_photo: null,
                    'last_message'=>$last_message
                ];
            }
        }

        usort($result,[$this,'sortChats']);
        $response = new ApiResponse($result, null, null);
        return response()->json($response, $this->successStatus);
    }

    public function deleteChat(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'chat_id'=>'required'
        ]);

        if ($validator->fails()) {
            $response = new ApiResponse(null, $validator->errors(), 'Validation errors');
            return response()->json($response, 400);
        }
        $user = Auth::user();
        $data = $request->all();
        $chat = Chat::find($data['chat_id']);
        if ($chat) {
            if ($chat->user1 == $user->id || $chat->user2 == $user->id) {
                foreach ($chat->messages as $message) {
                    $message->delete();
                }
                $chat->delete();
                $response = new ApiResponse(['Chat has deleted'], null, null);
                return response()->json($response, $this->successStatus);
            }else{
                $response = new ApiResponse(null, null, ['Access error!']);
                return response()->json($response, $this->successStatus);
            }
        }else{
            $response = new ApiResponse(null, null, ['Chat not found!']);
            return response()->json($response, $this->successStatus);
        }
    }

    private function sortChats($a, $b) {
        if ($a['last_message']->id == $b['last_message']->id) {
            return 0;
        }
        return ($a['last_message']->id > $b['last_message']->id) ? -1 : 1;
    }
}

