<?php

namespace App\Http\Controllers\API;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ComplimentController extends Controller
{
    public function makeCompliment(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'friend_id'=>'required|exists:users,id',
            'sticker_id'=>'required|exists:stickers,id',
            'text'=>'string'
        ]);
        $data = $request->all();
        if ($validator->fails()) {
            $response = new ApiResponse(null, $validator->errors(), 'Validation errors');
            return response()->json($response, 400);
        }
        $user = Auth::user();
        $vip = $user->vip()->latest()->first();
        if (!$vip || strtotime($vip->end_at) <= strtotime(date('Y-m-d H:i:s'))) {
            $response = new ApiResponse(null, null, ["You haven't vip status!"]);
            return response()->json($response, 400);
        }
        $friend = User::find($data['friend_id']);
        if ($friend->id == $user->id) {
            $response = new ApiResponse(null, null, ["You can't make compliment yourself!"]);
            return response()->json($response, 400);
        }
        $blacklist = $friend->blacklist()->where('block_id',$user->id)->first();
        if ($blacklist) {
            $response = new ApiResponse(null, null, ["You are in blacklist!"]);
            return response()->json($response, 400);
        }
        $exists_compliment = $friend->compliments()->withTrashed()->where('friend_id', $user->id)->first();
        if ($exists_compliment) {
            $response = new ApiResponse(null, null, ["You already made compliment to this user!"]);
            return response()->json($response, 400);
        }
        if (!array_key_exists('text',$data)) {
            $data['text'] = null;
        }
        $compliment = $friend->compliments()->create([
            'friend_id'=>$user->id,
            'sticker_id'=>$data['sticker_id'],
            'text'=>$data['text']
        ]);
        $response = new ApiResponse($compliment,null,null);
        return response()->json($response, 200);
    }

    public function deleteCompliment(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'compliment_id'=>'required|exists:compliments,id'
        ]);
        if ($validator->fails()) {
            $response = new ApiResponse(null, $validator->errors(), 'Validation errors');
            return response()->json($response, 400);
        }
        $user = Auth::user();
        $compliment = $user->compliments()->find($request->input('compliment_id'));
        if (!$compliment) {
            $response = new ApiResponse(null, null, ["Compliment not found!"]);
            return response()->json($response, 400);
        }
        $compliment->delete();
        $response = new ApiResponse(['Compliment has deleted!'],null,null);
        return response()->json($response, 200);
    }

    public function getCompliments(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id'=>'required|exists:users,id'
        ]);
        if ($validator->fails()) {
            $response = new ApiResponse(null, $validator->errors(), 'Validation errors');
            return response()->json($response, 400);
        }
        $compliments = User::find($request->input('user_id'))->compliments;
        $response = new ApiResponse($compliments,null,null);
        return response()->json($response, 200);
    }
}
