<?php

namespace App\Http\Controllers\API;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class FilterController extends Controller
{
    public $successStatus = 200;

    public function filterUsers(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'zodiacs'=>'required',
            'sex'=>'required',
            'latitude'=>'required',
            'longitude'=>'required',
            'radius'=>'required'
        ]);

        if ($validator->fails()) {
            $response = new ApiResponse(null, $validator->errors(), 'Validation errors');
            return response()->json($response, 400);
        }
        $data = $request->all();
        $current_user = Auth::user();
        $users = User::where('is_admin', 0)->where('id', '!=', $current_user->id);
        switch ($data['sex']) {
            case 1:
                $users = $users->where('sex','Мужской');
                break;
            case 2:
                $users = $users->where('sex','Женский');
                break;
        }
        if ( array_key_exists('years_from',$data)) {
            $need_date = date('Y-m-d',strtotime(date('Y-m-d')."-".$data['years_from']." years"));
            $users = $users->whereDate('birthdate','<=',$need_date);
        }
        if (array_key_exists('years_to',$data)) {
            $need_date = date('Y-m-d',strtotime(date('Y-m-d')."-".$data['years_to']." years"));
            $users = $users->whereDate('birthdate','>=',$need_date);
        }
        $translate_zodiacs = ['aries','taurus','gemini','cancer','leo','virgo','libra','scorpio','sagittarius','capricorn','aquarius','pisces'];
        $check_zodiac = [];
        foreach ($data['zodiacs'] as $zodiac) {
            $check_zodiac[] = $translate_zodiacs[$zodiac];
        }
        $users = $users->get();
        $result = [];
        $current_coordinates = $current_user->coordinates;
        $current_coordinates->latitude = $data['latitude'];
        $current_coordinates->longitude = $data['longitude'];
        $current_coordinates->save();
        foreach ($users as $user) {
            if (in_array($user->zodiac->name, $check_zodiac)) {
                $coordinantes = $user->coordinates;
                if($this->getDistance($coordinantes->latitude,$coordinantes->longitude,$current_coordinates->latitude, $current_coordinates->longitude) <= $data['radius']){
                    $photo = $user->photo()->where('is_avatar',1)->where('moderated',1)->first();
                    if ($photo) {
                        $user->photo = url('/storage/avatars/'.$photo->name_photo);
                    }else{
                        $user->photo = url('/assets/img/no_photo.jpg');
                    }
                    $result[] = $user;
                }
            }
        }
        $response = new ApiResponse($result, null, null);
        return response()->json($response, $this->successStatus);

    }

    private  function getDistance( $latitude1, $longitude1, $latitude2, $longitude2 ) {
        $earth_radius = 6371;

        $dLat = deg2rad( $latitude2 - $latitude1 );
        $dLon = deg2rad( $longitude2 - $longitude1 );

        $a = sin($dLat/2) * sin($dLat/2) + cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * sin($dLon/2) * sin($dLon/2);
        $c = 2 * asin(sqrt($a));
        $d = $earth_radius * $c;

        return $d;
    }

}
