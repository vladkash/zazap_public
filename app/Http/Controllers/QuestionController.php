<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;

class QuestionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    public function questionDelete($id)
    {
        $question = Question::find($id);
        $question->delete();
        return redirect('/questions');
    }

    public function sendMessage(Request $request)
    {
        $data = $request->all();
        $question = Question::find($data['question_id']);
        $question->messages()->create([
            'is_admin'=>1,
            'readed'=>0,
            'message'=>$data['message']
        ]);
    }

    public function changeStatus(Request $request)
    {
        $data = $request->all();
        $question = Question::find($data['question_id']);
        $question->status = $data['status'];
        $question->save();
        return redirect('/questions');
    }
}
