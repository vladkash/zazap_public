<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Placemark;

class PlacemarkController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    public function savePlacemark(Request $request, $id){
        $placemark = Placemark::find($id);
        $data = $request->all();

        if ($data['name'] != $placemark->name) {
            $request->validate([
                'name'=>'required'
            ]);
            $placemark->name = $data['name'];
        }
        if ($data['status'] != $placemark->status) {
            $request->validate([
                'status'=>'required'
            ]);
            $placemark->status = $data['status'];
        }
        if ($data['latitude'] != $placemark->latitude) {
            $request->validate([
                'latitude'=>'required'
            ]);
            $placemark->latitude = $data['latitude'];
        }
        if ($data['longitude'] != $placemark->longitude) {
            $request->validate([
                'longitude'=>'required'
            ]);
            $placemark->longitude = $data['longitude'];
        }

        if(!empty($request->file('image'))){
            if ($placemark->image != null) {
                unlink(public_path('storage/placemarkers/'.$placemark->image));
            }
            $guessExtension = $request->file('image')->guessExtension();
            $request->file('image')->storeAs('public/placemarkers', 'placemark'.$placemark->id.'.'.$guessExtension );
            $placemark->image = 'placemark'.$placemark->id.'.'.$guessExtension;
        }

        $placemark->save();
        return redirect()->back();
    }

    public function placemarkDelete($id){
        $placemark = Placemark::find($id);
        unlink(public_path('storage/placemarkers/'.$placemark->image));
        $placemark->delete();
        return redirect('/placemarks');
    }

    public function addPlacemark(Request $request){
        $request->validate([
            'name'=>'required',
            'status'=>'required',
            'latitude'=>'required',
            'longitude'=>'required'
        ]);
        $data = $request->all();
        $placemark = Placemark::create([
            'name'=>$data['name'],
            'status'=>$data['status'],
            'latitude'=>$data['latitude'],
            'longitude'=>$data['longitude']
        ]);
        if(!empty($request->file('image'))){
            $guessExtension = $request->file('image')->guessExtension();
            $request->file('image')->storeAs('public/placemarkers', 'placemark'.$placemark->id.'.'.$guessExtension );
            $placemark->image = 'placemark'.$placemark->id.'.'.$guessExtension;
            $placemark->save();
        }
        return redirect('/placemarks');
    }

    public function placemarkImageDelete($id){
        $placemark = Placemark::find($id);
        if($placemark->image != null){
            unlink(public_path('storage/placemarkers/'.$placemark->image));
            $placemark->image = null;
            $placemark->save();
        }
        return redirect()->back();
    }
}
