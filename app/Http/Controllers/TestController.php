<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class TestController extends Controller
{
    public function postSearch(Request $request){
        $birthdate_from = '1960-01-01';
        $birthdate_to = date('Y-m-d');
        $register_from = '2018-01-01';
        $register_to = date('Y-m-d');
        $id_from = 1;
        $id_to = User::max('id');
        $data = $request->all();
        if(!empty($data['birhdate_from'])){
            $birthdate_from = date('Y-m-d',strtotime($data['birhdate_from']));
        }
        if(!empty($data['birthdate_before'])){
            $birthdate_to = date('Y-m-d',strtotime($data['birthdate_before']));
        }
        if(!empty($data['register_from'])){
            $register_from = date('Y-m-d',strtotime($data['register_from']));
        }
        if(!empty($data['register_before'])){
            $register_to = date('Y-m-d',strtotime($data['register_before']));
        }
        if(!empty($data['id_from'])){
            $id_from = $data['id_from'];
        }
        if(!empty($data['id_before'])){
            $id_to =$data['id_before'];
        }
        $users = User::where('is_admin','!=',1)
            ->where('id','>=',$id_from)
            ->where('id','<=',$id_to)
            ->where('birthdate','>=',$birthdate_from)
            ->where('birthdate','<=',$birthdate_to)
            ->whereDate('created_at','>=',$register_from)
            ->whereDate('created_at','<=',$register_to)->get();

        $result_users = [];
        if(!empty($data['search_phrase'])){
            foreach ($users as $user)
            {
                if(strpos(mb_strtolower($user->name),mb_strtolower($data['search_phrase'])) !== false ||
                    strpos(mb_strtolower ($user->email),mb_strtolower($data['search_phrase'])) !== false){
                    $result_users[] = $user;
                }
            }
        }else{
            $result_users = $users;
        }
        $result_coordinates = [];
        $names = [];
        dump($result_users);
        foreach ($result_users as $result_user){
            $result_coordinates[] = $result_user->coordinates;
            $names[$result_user->id] = $result_user->name;
        }
        return view('search',['users'=>$result_user, 'coordinates'=>$result_coordinates, 'names'=>$names]);
    }
}
