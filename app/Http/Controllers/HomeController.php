<?php

namespace App\Http\Controllers;

use App\Coordinate;
use App\Photo;
use App\Question;
use App\Sale;
use App\Sticker;
use App\User;
use Illuminate\Http\Request;
use App\Placemark;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function getDashboard()
    {
        $all_users = User::count();
        $month_users = User::whereDate('created_at', '>=', date('Y-m-d', strtotime('-30 days')))->whereDate('created_at','<=',date('Y-m-d'))->count();
        $week_users = User::whereDate('created_at', '>=', date('Y-m-d', strtotime('-7 days')))->whereDate('created_at','<=',date('Y-m-d'))->count();
        $all_sales = Sale::where('status', 'Зачислено')->count();
        $vip_sales = Sale::where('type','VIP')->where('status', 'Зачислено')->count();
        $sticker_sales = Sale::where('type','!=','VIP')->where('status', 'Зачислено')->count();

        return view('dashboard.index',['all_users'=>$all_users,'month_users'=>$month_users,'week_users'=>$week_users,'all_sales'=>$all_sales,'vip_sales'=>$vip_sales,'sticker_sales'=>$sticker_sales]);
    }

    //users methods
    public function getUsers()
    {
        $users = User::where('is_admin',0)->get();
        $names = [];
        foreach ($users as $user) {
            $names[$user->id] = $user->name;
        }
        $coordinates = Coordinate::all();
        $users = User::where('is_admin',0)->orderBy('id','desc')->paginate(15);

        return view('users.index',['users'=>$users,'coordinates'=>$coordinates,'names'=>$names]);
    }
    public function getUser($id)
    {
        $user = User::find($id);
        $photo = $user->photo()->where('is_avatar',1)->first();
        if(!$photo){
            $photo = url('/assets/img/user.jpg');
        }else{
            $photo = url('/storage/avatars/'.$photo->name_photo);
        }
        $sales = $user->sales;
        return view('users.edit',['user'=>$user, 'photo'=>$photo,'sales'=>$sales]);
    }
    public function getAddUser(){
        return view('users.add');
    }

    //placemark methods
    public function getPlacemarks()
    {
        $map_placemarks = PlaceMark::all();
        $placemarks = Placemark::orderBy('id','desc')->paginate(15);
        return view('placemarks.index',['placemarks'=>$placemarks,'map_placemarks'=>$map_placemarks]);
    }
    public function getPlacemark($id)
    {
        $placemark = Placemark::find($id);
        return view('placemarks.edit',['placemark'=>$placemark]);
    }
    public function getAddPlacemark()
    {
        return view('placemarks.add');
    }

    //sales methods
    public function getSales()
    {
        $sales = Sale::orderBy('id','desc')->paginate(15);
        return view('sales.index',['sales'=>$sales]);
    }

    //photos method
    public function getPhotos()
    {
        $photos = Photo::where('moderated',0)->paginate(5);
        $route = \Request::route()->getName();
        return view('photos.index',['photos'=>$photos, 'route'=>$route]);
    }
    public function getPhotosHistory()
    {
        $photos = Photo::onlyTrashed()->where('moderated', 0)->paginate(5);
        $route = \Request::route()->getName();
        return view('photos.index',['photos'=>$photos, 'route'=>$route]);
    }

    //questions methods
    public function getQuestions()
    {
        $questions = Question::orderBy('id','desc')->paginate(15);
        foreach ($questions as $question) {
            $message = $question->messages()->latest()->first();
            $question->first_message = $message->message;
        }
        return view('questions.index',['questions'=>$questions]);
    }
    public function getQuestion($id)
    {
        $question = Question::find($id);
        if ($question->status == 'Новый') {
            $question->status = 'В обработке';
            $question->save();
        }
        $messages = $question->messages;
        foreach ($messages as $message) {
            if (!$message->is_admin && !$message->readed) {
                $message->readed = 1;
                $message->save();
            }
        }
        $user = User::find($question->user_id);
        $photo = $user->photo()->where('is_avatar',1)->first();
        if ($photo) {
            $user->photo = url('/storage/avatars/'.$photo->name_photo);
        }else{
            $user->photo = url('/assets/img/user.jpg');
        }
        return view('questions.chat',['messages'=>$messages, 'user'=>$user, 'question'=>$question]);
    }

    public function getStickers()
    {
        $stickers = Sticker::paginate(15);
        return view('stickers.index',['stickers'=>$stickers]);
    }

    public function getAddSticker()
    {
        return view('stickers.add');
    }

    public function getEditSticker($id)
    {
        $sticker = Sticker::find($id);
        return view('stickers.edit',['sticker' => $sticker]);
    }

}
