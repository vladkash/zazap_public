<?php

namespace App\Http\Controllers;

use App\Photo;
use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }


    public function saveUser(Request $request, $id){

        $user = User::find($id);
        $data = $request->all();
        if ($user->name != $data['name']) {
            $request->validate([
                'name' => 'unique:users'
            ]);
            $user->name = $data['name'];
        }
        if ($user->phone != $data['phone']) {
            $request->validate([
                'phone'=>'unique:users'
            ]);
            $user->phone = $data['phone'];
        }
        if ($user->email != $data['email']) {
            $request->validate([
                'email'=>'email|unique:users',
            ]);
            $user->email = $data['email'];
        }
        $user->birthdate = date('Y-m-d',strtotime($data['birthdate']));
        $user->save();
        if(!empty($request->file('photo'))){
            $request->validate([
                'image'=>'image'
            ]);
            $photo = $user->photo()->where('is_avatar',1)->first();
            if($photo){
                if (file_exists(public_path('storage/avatars/'.$photo->name_photo))) {
                    unlink(public_path('storage/avatars/'.$photo->name_photo));
                }
                $guessExtension = $request->file('photo')->guessExtension();
                $request->file('photo')->storeAs('public/avatars', 'user'.$photo->id.'avatar'.'.'.$guessExtension );
                $photo->name_photo = 'user'.$photo->id.'avatar'.'.'.$guessExtension;
                $photo->save();
            }else{
                $photo_id = Photo::max('id')+1;
                $guessExtension = $request->file('photo')->guessExtension();
                $request->file('photo')->storeAs('public/avatars', 'user'.$photo_id.'avatar'.'.'.$guessExtension );
                $user->photo()->create([
                    'name_photo'=>'user'.$photo_id.'avatar'.'.'.$guessExtension,
                    'moderated'=>1,
                    'is_avatar'=>1
                ]);
            }

        }

        return redirect()->back();
    }

    public function addUser(Request $request){
        $request->validate([
            'name' => 'required|unique:users',
            'phone'=>'required|unique:users',
            'email'=>'required|email|unique:users',
            'birthdate'=>'required',
            'latitude'=>'required',
            'longitude'=>'required',
            'photo'=>'required|image'
        ]);
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create([
            'name'=>$input['name'],
            'email'=>$input['email'],
            'sex'=>$input['sex'],
            'is_admin'=>0,
            'phone'=>$input['phone'],
            'birthdate'=> date('Y-m-d', strtotime($input['birthdate'])),
            'lastvisit'=>date('Y-m-d'),
            'password'=>$input['password']
        ]);
        $user->coordinates()->create([
            'longitude'=>$input['longitude'],
            'latitude'=>$input['latitude']
        ]);
        $guessExtension = $request->file('photo')->guessExtension();
        $photo_id = Photo::max('id') + 1;
        $request->file('photo')->storeAs('public/avatars', 'user'.$photo_id.'avatar'.'.'.$guessExtension );
        $user->photo()->create([
            'name_photo'=>'user'.$photo_id.'avatar'.'.'.$guessExtension,
            'is_avatar'=> 1,
            'moderated'=>1
        ]);

        return redirect('/users');
    }

    public function userDelete($id){
        $user = User::find($id);
        $user->coordinates->delete();
        $photos = $user->photo;
        foreach ($photos as $photo) {
            if (file_exists(public_path('storage/avatars/'.$photo->name_photo))) {
                unlink(public_path('storage/avatars/'.$photo->name_photo));
            }
            $photo->forceDelete();
        }
        $user->delete();
        return redirect('/users');
    }
}
