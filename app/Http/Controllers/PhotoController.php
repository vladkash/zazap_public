<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Photo;

class PhotoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    public function acceptPhoto($id)
    {
        $photo = Photo::withTrashed()->find($id);
        if($photo->trashed()) {
            $photo->restore();
        }
        $photo->moderated = 1;
        $photo->save();
        return redirect('/photos');
    }

    public function rejectPhoto($id)
    {
        $photo = Photo::withTrashed()->find($id);

        if($photo->trashed()){
            $path = public_path('/storage/avatars/' . $photo->name_photo);
            if (file_exists($path)) {
                unlink($path);
            }
            $photo->forceDelete();
        }else {
            $photo->delete();
        }

        return redirect('/photos');
    }
}
