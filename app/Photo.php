<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Photo extends Model
{
    use SoftDeletes;

    protected $guarded = [];
    protected $dates = ['deleted_at'];

    public function likes()
    {
        return $this->hasMany('App\Like');
    }

    public function reports()
    {
        return $this->hasMany('App\Report');
    }
}
