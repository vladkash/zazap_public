<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MessageContent extends Model
{
    protected $guarded = [];

    public function message()
    {
        return $this->belongsTo('App\Message');
    }
}
