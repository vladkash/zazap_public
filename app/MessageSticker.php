<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MessageSticker extends Model
{
    protected $guarded = [];

    public function sticker()
    {
        return $this->belongsTo('App\Sticker');
    }
}
