<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $guarded = [];

    public function content()
    {
        return $this->hasOne('App\MessageContent');
    }

    public function sticker()
    {
        return $this->hasOne('App\MessageSticker');
    }
}
