<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable, \Intervention\Zodiac\EloquentZodiacTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    /*protected $fillable = [
        'name', 'email', 'password',
    ];*/
    protected $guarded = [];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','is_admin'
    ];

    public function coordinates()
    {
        return $this->hasOne('App\Coordinate');
    }
    public function photo()
    {
        return $this->hasMany('App\Photo');
    }

    public function sales()
    {
        return $this->hasMany('App\Sale');
    }

    public function questions()
    {
        return $this->hasMany('App\Question');
    }

    public function blacklist()
    {
        return $this->hasMany('App\BlackList',null);
    }

    public function recipiented_messages()
    {
        return $this->hasMany('App\Message','recipient_id');
    }

    public function subscribes()
    {
        return $this->hasMany('App\Subscribe');
    }

    public function subscribers()
    {
        return $this->hasMany('App\Subscribe', 'subscribe_id');
    }

    public function vip()
    {
        return $this->hasMany('App\UserVip');
    }

    public function compliments()
    {
        return $this->hasMany('App\Compliment');
    }

    public function likes()
    {
        return $this->hasMany('App\Like');
    }

    public function reports()
    {
        return $this->hasMany('App\Report');
    }
}
