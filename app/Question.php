<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $guarded = [];

    public function messages()
    {
        return $this->hasMany('App\QuestionMessage');
    }
}
