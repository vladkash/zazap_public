@extends("layouts.admin")

@section("content")
    <style>
        .cartochka_maps p {
            margin-top: 20px;
            margin-bottom: 10px;
            font-size: 16px;
        }
    </style>
    <div class="content content_active">
        <div class="wrap">
            <section class="main">
                <h2>метки</h2>
                <div class="this_yii">
                    <div class="one_yi">
                        <a href="{{route('addplacemark')}}" class="btn btn-primary">Добавить новую метку</a>
                    </div>
                    <div class="two_yi">
                        <li><a href="{{route('placemarks')}}">Карта</a></li>
                        <li><a class="active" href="#">Список</a></li>
                    </div>
                </div>
                <div class="pagination_top">
                    <h3 href="" class="tag_strelka"><a href="{{route('placemarks')}}">Метки</a></h3>
                    <h3 class="tag_strelka"><a href="{{route('addplacemark')}}">Добавить новую метку</a> </h3>
                </div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div class="col-md-10">
                    <div class="cartochka_maps">
                        <form action="{{route('addplacemark_p')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <p>Название метки</p>
                            <input name="name" type="text" class="form-control">
                            <p>Статус метки</p>
                            <select name="status" class="custom-select">
                                <option selected value="Активна">Активна</option>
                                <option value="Не активна">Не активна</option>
                            </select>
                            <p>Изображение метки:</p>
                            <input type="file" name="image" class="form-control-file">
                            <p>Расположение метки:</p>
                            <div id="map" style="width:100%;height: 400px"></div>
                            <div class="row">
                                <div class="col-md-6">
                                    <p>Широта</p>
                                    <input name="latitude" id="lat" type="text" class="form-control">
                                </div>
                                <div class="col-md-6">
                                    <p>Долгота</p>
                                    <input name="longitude" id="lng" type="text" class="form-control">
                                </div>
                            </div>
                            <div class="btn_cart_maps">
                                <input type="submit" class="btn btn-primary btn-lg btn-block" value="Добавить метку">
                            </div>
                        </form>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection

@section("js")
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
    <script>
        ymaps.ready(init);

        function init() {
            var myPlacemark,
                myMap = new ymaps.Map('map', {
                    center: [55.753994, 37.622093],
                    zoom: 9
                }, {
                    searchControlProvider: 'yandex#search'
                });

            // Слушаем клик на карте.
            myMap.events.add('click', function (e) {
                var coords = e.get('coords');
                console.log(coords);
                $('#lat').val(coords[0]);
                $('#lng').val(coords[1]);
                // Если метка уже создана – просто передвигаем ее.
                if (myPlacemark) {
                    myPlacemark.geometry.setCoordinates(coords);
                }
                // Если нет – создаем.
                else {
                    myPlacemark = createPlacemark(coords);
                    myMap.geoObjects.add(myPlacemark);
                    // Слушаем событие окончания перетаскивания на метке.
                    myPlacemark.events.add('dragend', function () {
                        var coordinates = myPlacemark.geometry.getCoordinates();
                        $('#lat').val(coordinates[0]);
                        $('#lng').val(coordinates[1]);
                        getAddress(myPlacemark.geometry.getCoordinates());
                    });
                }
                getAddress(coords);
            });

            // Создание метки.
            function createPlacemark(coords) {
                return new ymaps.Placemark(coords, {
                    iconCaption: 'поиск...'
                }, {
                    preset: 'islands#redDotIconWithCaption',
                    draggable: true
                });
            }

            // Определяем адрес по координатам (обратное геокодирование).
            function getAddress(coords) {
                myPlacemark.properties.set('iconCaption', 'поиск...');
                ymaps.geocode(coords).then(function (res) {
                    var firstGeoObject = res.geoObjects.get(0);

                    myPlacemark.properties
                        .set({
                            // Формируем строку с данными об объекте.
                            iconCaption: [
                                // Название населенного пункта или вышестоящее административно-территориальное образование.
                                firstGeoObject.getLocalities().length ? firstGeoObject.getLocalities() : firstGeoObject.getAdministrativeAreas(),
                                // Получаем путь до топонима, если метод вернул null, запрашиваем наименование здания.
                                firstGeoObject.getThoroughfare() || firstGeoObject.getPremise()
                            ].filter(Boolean).join(', '),
                            // В качестве контента балуна задаем строку с адресом объекта.
                            balloonContent: firstGeoObject.getAddressLine()
                        });
                });
            }
        }

    </script>
@endsection
