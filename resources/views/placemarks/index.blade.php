@extends("layouts.admin")

@section("content")
    <link rel="stylesheet" href="{{asset('/assets/css/users.css')}}">
    <div class="content content_active" id="users_map">
        <div class="wrap">
            <div class="main">
                <h2>метки</h2>
                <div class="this_yii">
                    <div class="one_yi">
                        <a href="{{route('addplacemark')}}" class="btn btn-primary">Добавить новую метку</a>
                    </div>
                    <div class="two_yi">
                        <li><a class="active" href="#">Карта</a></li>
                        <li><a id="show_users_list" href="#">Список</a></li>
                    </div>
                </div>
                <h3 class="tag_strelka"><a href="{{route('placemarks')}}">Метки</a></h3>
                <div id="users_map_container"></div>
                <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU"
                        type="text/javascript"></script>
                <script type="text/javascript">
                    ymaps.ready(function () {
                        var map = new ymaps.Map("users_map_container", {
                            center: [55.76, 37.64],
                            zoom: 10,
                            controls: []
                        });
                        if (map) {
                            ymaps.modules.require(['Placemark', 'Circle'], function (Placemark, Circle) {
                                var placemark = new Placemark([55.37, 35.45]);
                            });
                        }
                                @foreach($placemarks as $placemark)
                        var myPlacemark = new ymaps.Placemark([{{$placemark->latitude}}, {{$placemark->longitude}}], {
                                balloonContentBody: [
                                    '<a href="{{route('placemark',['id'=>$placemark->id])}}" target="_blank">{{$placemark->name}}</a>',
                                ].join('')
                            }, {
                                @if($placemark->image == null)
                                    preset: 'islands#redDotIcon',
                                @else
                                    iconLayout: 'default#image',
                                    iconImageHref: '/storage/placemarkers/{{$placemark->image}}',
                                    iconImageSize: [30, 30],
                                @endif
                            });
                        map.geoObjects.add(myPlacemark);
                        @endforeach
                    });
                </script>
            </div>
        </div>
    </div>

    {{--Other block--}}

    <div class="content content_active" id="users_list">
        <div class="wrap">
            <section class="main">
                <h2>метки</h2>
                <div class="this_yii">
                    <div class="one_yi">
                        <a href="{{route('addplacemark')}}" class="btn btn-primary">Добавить новую метку</a>
                    </div>
                    <div class="two_yi">
                        <li><a id="show_users_map" href="#">Карта</a></li>
                        <li><a class="active" href="#">Список</a></li>
                    </div>
                </div>
                <h3 class="tag_strelka"><a href="{{route('placemarks')}}">Метки</a></h3>
                <table class="table table-borderless table_all_spisok">
                    <thead>
                    <tr>
                        <th scope="col">№</th>
                        <th scope="col">Название</th>
                        <th scope="col">Статус метки</th>
                        <th scope="col">Gps</th>
                        <td></td>
                        <td></td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($placemarks as $placemark)
                        <tr class="column_table">
                            <th scope="row">{{$placemark->id}}</th>
                            <td>{{$placemark->name}}</td>
                            <td>
                                @if($placemark->status == 'Активна')
                                    <span class="success_c">{{$placemark->status}}</span>
                                @else
                                    <span class="yellow_c">{{$placemark->status}}</span>
                                @endif
                            </td>
                            <td>{{$placemark->latitude}}, {{$placemark->longitude}}</td>
                            <td class="answer_block">
                                <a href="{{route('placemark',['id'=>$placemark->id])}}">Редактировать</a>
                            </td>
                            <td class="answer_close">
                                <a href="{{route('placemarkdelete',['id'=>$placemark->id])}}">X</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{$placemarks->links('layouts.pagination')}}
            </section>
        </div>
    </div>
@endsection
