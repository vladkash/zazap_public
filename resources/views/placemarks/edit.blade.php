@extends("layouts.admin")

@section("content")
    <style>
        .cartochka_maps p {
            margin-top: 20px;
            margin-bottom: 10px;
            font-size: 16px;
        }
    </style>
    <div class="content content_active">
        <div class="wrap">
            <section class="main">
                <h2>метки</h2>
                <div class="this_yii">
                    <div class="one_yi">
                        <a href="{{route('addplacemark')}}" class="btn btn-primary">Добавить новую метку</a>
                    </div>
                    <div class="two_yi">
                        <li><a href="{{route('placemarks')}}">Карта</a></li>
                        <li><a class="active" href="#">Список</a></li>
                    </div>
                </div>
                <div class="pagination_top">
                    <h3 href="" class="tag_strelka"><a href="{{route('placemarks')}}">Метки</a></h3>
                    <h3 class="tag_strelka"><a href="{{route('placemark',['id'=>$placemark->id])}}">Редактировать метку</a> </h3>
                </div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div class="col-md-10">
                    <div class="cartochka_maps">
                        <form action="{{route('placemarksave',['id'=>$placemark->id])}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <p>Название метки</p>
                            <input name="name" type="text" class="form-control" value="{{$placemark->name}}">
                            <p>Статус метки</p>
                            <select name="status" class="custom-select">
                                <option selected value="{{$placemark->status}}">{{$placemark->status}}</option>
                                <option value="Активна">Активна</option>
                                <option value="Не активна">Не активна</option>
                            </select>
                            <p>Изображение метки:</p>
                            @if($placemark->image != null)
                                <img style="width: 30px; height: 30px" src="{{url('/storage/placemarkers/'.$placemark->image)}}" alt="">
                            @endif
                            <input type="file" name="image" class="form-control-file">
                            <a href="{{route('placemarkimagedelete',['id'=>$placemark->id])}}" class="btn btn-danger btn_delete mt-3">Удалить изображение</a>

                            <p>Расположение метки:</p>
                            <div id="map" style="width:100%;height: 400px"></div>
                            <div class="row">
                                <div class="col-md-6">
                                    <p>Широта</p>
                                    <input name="latitude" id="lat" type="text" class="form-control" value="{{$placemark->latitude}}">
                                </div>
                                <div class="col-md-6">
                                    <p>Долгота</p>
                                    <input name="longitude" id="lng" type="text" class="form-control" value="{{$placemark->longitude}}">
                                </div>
                            </div>
                            <div class="btn_cart_maps">
                                <div class="col-md-6">
                                    <a href="{{route('placemarkdelete',['id'=>$placemark->id])}}" class="btn btn-primary btn_delete btn-block">Удалить</a>
                                </div>
                                <div class="col-md-6">
                                    <input type="submit" class="btn btn-primary btn-block" value="Сохранить">
                                </div>
                                </div>
                        </form>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection

@section("js")
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
    <script>
        ymaps.ready(init);

        function init() {
            var myPlacemark,
                myMap = new ymaps.Map('map', {
                    center: [55.753994, 37.622093],
                    zoom: 9
                }, {
                    searchControlProvider: 'yandex#search'
                });
            myPlacemark = new ymaps.Placemark([{{$placemark->latitude}},{{$placemark->longitude}}], {
                iconCaption: '{{$placemark->name}}'
            }, {
                @if($placemark->image == null)
                preset: 'islands#redDotIconWithCaption',
                @else
                iconLayout: 'default#image',
                iconImageHref: '/storage/placemarkers/{{$placemark->image}}',
                iconImageSize: [30, 30],
                @endif
                draggable: true
            });
            myMap.geoObjects.add(myPlacemark);
            myPlacemark.events.add('dragend', function () {
                var coordinates = myPlacemark.geometry.getCoordinates();
                $('#lat').val(coordinates[0]);
                $('#lng').val(coordinates[1]);
            });
            // Слушаем клик на карте.
            myMap.events.add('click', function (e) {
                var coords = e.get('coords');
                console.log(coords);
                // Если метка уже создана – просто передвигаем ее.
                if (myPlacemark) {
                    myPlacemark.events.add('dragend', function () {
                        var coordinates = myPlacemark.geometry.getCoordinates();
                        $('#lat').val(coordinates[0]);
                        $('#lng').val(coordinates[1]);
                    });
                    myPlacemark.geometry.setCoordinates(coords);
                    $('#lat').val(coords[0]);
                    $('#lng').val(coords[1]);
                }
            });
        }

    </script>
@endsection