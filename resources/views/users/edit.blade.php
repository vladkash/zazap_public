@extends("layouts.admin")

@section("content")
    <style>
        .cartochka_maps p {
            margin-top: 20px;
            margin-bottom: 10px;
            font-size: 12px;
        }
    </style>
    <div class="content content_active">
        <div class="wrap">
            <section class="main">
                <h2>все пользователи</h2>
                <div class="this_yii">
                    <div class="one_yi">
                        <a href="{{route('adduser')}}" class="btn btn-primary">Добавить пользователя</a>
                    </div>
                    <div class="two_yi">
                        <li><a href="{{route('users')}}">Карта</a></li>
                        <li><a class="active" href="#">Список</a></li>
                    </div>
                </div>
                <div class="pagination_top">
                    <h3 class="tag_strelka"><a href="{{route('users')}}">Все пользователи</a></h3>
                    <h3 class="tag_strelka"><a href="{{route('user',['id'=>$user->id])}}">Карта пользователя</a> </h3>
                </div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div class="maps_tables">
                    <div class="cartochka_maps">
                        <img src="{{$photo}}" alt="">
                        <form action="{{route('usersave',['id'=>$user->id])}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <p>Id пользователя</p>
                            {{$user->id}}
                            <p>Никнейм</p>
                            <input name="name" type="text" class="form-control" value="{{$user->name}}">
                            <p>Телефон:</p>
                            <input name="phone" type="text" class="form-control phone_pick" value="{{$user->phone}}">
                            <p>E-mail:</p>
                            <input name="email" type="email" class="form-control" value="{{$user->email}}">
                            <p>Дата рождения</p>
                            <input name="birthdate" type="text" class="form-control date_pick" value="{{date('d.m.Y',strtotime($user->birthdate))}}">
                            <p>Дата регистрации</p>
                            <h3>{{date('d.m.Y',strtotime($user->created_at))}}</h3>
                            <p>Последний визит:</p>
                            <h3>{{date('d.m.Y',strtotime($user->lastvisit))}}</h3>
                            <p>Фотография:</p>
                            <input type="file" name="photo" class="form-control-file">
                            <div class="btn_cart_maps">
                                <input type="submit" href="" class="btn btn-primary" value="Сохранить">
                                <a href="{{route('userdelete',['id'=>$user->id])}}" class="btn btn-primary btn_delete">Удалить</a>
                            </div>
                        </form>
                    </div>


                    <div class="this_two_sect">
                        <div class="balance">
                            <p>Баланс: 1000 руб.</p>
                        </div>
                        <table class="table tables_maps_user">

                            <thead>
                            <tr>
                                <th scope="col">тип покупки</th>
                                <th scope="col">дата, время</th>
                                <th scope="col">способ опл.</th>
                                <th scope="col">сумма транз.</th>
                                <th scope="col">Статус</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($sales as $sale)
                            <tr class="column_table">
                                <td>{{$sale->type}}</td>
                                <td>{{date('d.m.Y H:m',strtotime($sale->created_at))}}</td>
                                <td>{{$sale->method}}</td>
                                <td>{{$sale->amount}} РУБ.</td>
                                <td>
                                    @if($sale->status == 'Зачислено')
                                        <span class="success_c">Зачислено</span>
                                    @elseif($sale->status == 'Ожидание')
                                        <span class="yellow_c">Ожидание</span>
                                    @elseif($sale->status == 'Отменено')
                                        <span class="warning_c">Отменено</span>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>


                </div>

            </section>
        </div>

        <!--     <section class="news">
          <h2>Новости</h2>
        </section>
        <section class="contacts">
          <h2>Контакты</h2>
        </section>
        <section class="portfolio">
          <h2>Портфолио</h2>
        </section> -->
    </div>
@endsection
