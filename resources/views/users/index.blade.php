@extends("layouts.admin")

@section("content")
    <link rel="stylesheet" href="{{asset('/assets/css/users.css')}}">
    <div class="content content_active" id="users_map">
        <div class="wrap">
            <div class="main">
                <h2>все пользователи</h2>
                <div class="this_yii">
                    <div class="one_yi">
                        <a href="{{route('adduser')}}" class="btn btn-primary">Добавить пользователя</a>
                    </div>
                    <div class="two_yi">
                        <li><a class="active" href="#">Карта</a></li>
                        <li><a id="show_users_list" href="#">Список</a></li>
                    </div>
                </div>
                <h3 class="tag_strelka"><a href="{{route('users')}}">Все пользователи</a></h3>
                <div id="users_map_container"></div>
                <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU"
                        type="text/javascript"></script>
                <script type="text/javascript">
                    ymaps.ready(function () {
                        var map = new ymaps.Map("users_map_container", {
                            center: [55.76, 37.64],
                            zoom: 10,
                             controls: []
                        });
                        if (map) {
                            ymaps.modules.require(['Placemark', 'Circle'], function (Placemark, Circle) {
                                var placemark = new Placemark([55.37, 35.45]);
                            });
                        }
                        @foreach($coordinates as $coordinate)
                        var myPlacemark = new ymaps.Placemark([{{$coordinate->latitude}}, {{$coordinate->longitude}}], {
                                balloonContentBody: [
                                    '<a href="{{route('user',['id'=>$coordinate->user_id])}}" target="_blank">{{$names[$coordinate->user_id]}}</a>',
                                ].join('')
                            }, {
                                preset: 'islands#redDotIcon'
                            });
                        map.geoObjects.add(myPlacemark);
                        @endforeach
                    });
                </script>
            </div>
        </div>

        <!--     <section class="news">
          <h2>Новости</h2>
        </section>
        <section class="contacts">
          <h2>Контакты</h2>
        </section>
        <section class="portfolio">
          <h2>Портфолио</h2>
        </section> -->
    </div>

    {{--Other block--}}

    <div class="content content_active" id="users_list">
        <div class="wrap">
            <section class="main">
                <h2>все пользователи</h2>
                <div class="this_yii">
                    <div class="one_yi">
                        <a href="{{route('adduser')}}" class="btn btn-primary">Добавить пользователя</a>
                    </div>
                    <div class="two_yi">
                        <li><a id="show_users_map" href="#">Карта</a></li>
                        <li><a class="active" href="#">Список</a></li>
                    </div>
                </div>
                <h3 class="tag_strelka"><a href="{{route('users')}}">Все пользователи</a></h3>

                <div class="filter_spisok">
                    <div class="user_spisok">

                    </div>
                    <a class="btn btn-primary filter_link" data-toggle="collapse" href="#multiCollapseExample1" role="button" aria-expanded="false" aria-controls="multiCollapseExample1"><img src="{{asset('/assets/img/filter.png')}}" alt="">Фильтр</a>
                </div>

                <div class="collapse multi-collapse" id="multiCollapseExample1">
                    <div class="card card-body">
                        <p class="close_filter_s">{
                            Фильтр-поиск
                        </p>
                        <hr>

                        <form action="{{route('search_user')}}" class="form_spisok" method="post">
                            @csrf
                            <div class="row">
                                <div class="col-lg-3">
                                    <label for="basic-url">Поиск</label>
                                    <div class="input-group mb-3">
                                        <input type="text" name="search_phrase" class="form-control" id="basic-url"  aria-describedby="basic-addon3">
                                    </div>
                                    <div class="input-group mb-3">
                                        <div class="this_yii" style="width: 100%; margin-top: 0">
                                            <div class="one_yi" style="width: 100%">
                                                <input type="submit" class="btn btn-primary" value="Поиск" style="width: 100%">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <label for="basic-url">ID</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">ОТ</span>
                                        </div>
                                        <input type="text" name="id_from" class="form-control" aria-label="Amount (to the nearest dollar)">
                                    </div>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">ДО</span>
                                        </div>
                                        <input type="text" name="id_before" class="form-control" aria-label="Amount (to the nearest dollar)">
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <label for="basic-url">Дата рождения</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">ОТ</span>
                                        </div>
                                        <input type="text" name="birhdate_from" class="form-control date_pick" aria-label="Amount (to the nearest dollar)">
                                    </div>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">ДО</span>
                                        </div>
                                        <input type="text" name="birthdate_before" class="form-control date_pick" aria-label="Amount (to the nearest dollar)">
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <label for="basic-url">Дата регистрации</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">ОТ</span>
                                        </div>
                                        <input type="text" name="register_from" class="form-control date_pick" aria-label="Amount (to the nearest dollar)">
                                    </div>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">ДО</span>
                                        </div>
                                        <input type="text" name="register_before"  class="form-control date_pick" aria-label="Amount (to the nearest dollar)">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>


                <table class="table table-borderless table_all_spisok">
                    <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Никнейм</th>
                        <th scope="col">Email</th>
                        <th scope="col">Телефон</th>
                        <th scope="col">Дата рожд.</th>
                        <th scope="col">Посл.вход</th>
                        <th scope="col">Баланс</th>
                        <th scope="col">Покупки</th>
                        <th scope="col">Статус</th>
                        <td></td>
                        <td></td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        @if(!$user->is_admin)
                            <tr class="column_table">
                                <th scope="row">{{$user->id}}</th>
                                <td>{{$user->name}}</td>
                                <td>{{$user->email}}</td>
                                <td>{{$user->phone}}</td>
                                <td>{{date('d.m.Y',strtotime($user->birthdate))}}</td>
                                <td>{{date('d.m.Y',strtotime($user->lastvisit))}}</td>
                                <td>100руб.</td>
                                <td>5</td>
                                <td>Активен</td>
                                <td class="answer_block">
                                    <a href="{{route('user',['id'=>$user->id])}}">Редактировать</a>
                                </td>
                                <td class="answer_close">
                                    <a href="{{route('userdelete',['id'=>$user->id])}}">X</a>
                                </td>
                            </tr>
                    @endif
                    @endforeach
                    </tbody>
                </table>
                {{$users->links('layouts.pagination')}}
            </section>
        </div>
    </div>

@endsection
