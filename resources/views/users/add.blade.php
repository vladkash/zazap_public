@extends("layouts.admin")

@section("content")
    <style>
        .cartochka_maps p {
            margin-top: 20px;
            margin-bottom: 10px;
            font-size: 16px;
        }
    </style>
    <div class="content content_active">
        <div class="wrap">
            <section class="main">
                <h2>все пользователи</h2>
                <div class="this_yii">
                    <div class="one_yi">
                        <a href="{{route('adduser')}}" class="btn btn-primary">Добавить пользователя</a>
                    </div>
                    <div class="two_yi">
                        <li><a href="{{route('users')}}">Карта</a></li>
                        <li><a class="active" href="#">Список</a></li>
                    </div>
                </div>
                <div class="pagination_top">
                    <h3 href="" class="tag_strelka"><a href="{{route('users')}}">Все пользователи</a></h3>
                    <h3 class="tag_strelka"><a href="{{route('adduser')}}">Добавить пользователя</a> </h3>
                </div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div class="col-md-10">
                    <div class="cartochka_maps">
                        <form action="{{route('adduser_p')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <p>Никнейм</p>
                            <input name="name" type="text" class="form-control">
                            <p>Пол</p>
                            <select name="sex" class="custom-select">
                                <option selected>Выберите пол</option>
                                <option value="Мужской">Мужской</option>
                                <option value="Женский">Женский</option>
                            </select>
                            <p>Телефон:</p>
                            <input name="phone" type="text" class="form-control phone_pick">
                            <p>E-mail:</p>
                            <input name="email" type="email" class="form-control" value="">
                            <p>Дата рождения</p>
                            <input name="birthdate" type="text" class="form-control date_pick">
                            <p>Пароль</p>
                            <input name="password" type="password" class="form-control">
                            <p>Подтвердите пароль</p>
                            <input name="c_password" type="password" class="form-control">
                            <p>Фотография:</p>
                            <input type="file" name="photo" class="form-control-file">
                            <p>Начальное расположение пользователя</p>
                            <div id="map_canvas" style="width: 100%;height: 400px;"></div>
                            <input type="hidden" id="lat" name="latitude">
                            <input type="hidden" id="lng" name="longitude">
                            <div class="btn_cart_maps">
                                <input type="submit" id="map-button" class="btn btn-primary btn-lg btn-block" value="Добавить пользователя">
                            </div>
                        </form>
                    </div>
                </div>
            </section>
        </div>
        <!--     <section class="news">
          <h2>Новости</h2>
        </section>
        <section class="contacts">
          <h2>Контакты</h2>
        </section>
        <section class="portfolio">
          <h2>Портфолио</h2>
        </section> -->
    </div>
@endsection

@section("js")
    <script>


        function initMap() {
            var mapLatlng = new google.maps.LatLng(55.76, 37.64);
            var zoom = 10;
            var mapOptions = {
                zoom: parseInt(zoom),
                center: mapLatlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
            marker = new google.maps.Marker({
                position: mapLatlng,
                map: map,
                draggable: true
            });
            $('#map-button').click(function(){
                var locatedAddress = marker.getPosition();
                $('#lat').val(locatedAddress.lat());
                $('#lng').val(locatedAddress.lng());
            });
        }
    </script>
    <script defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDppcYXXU2XNuv_YkzNaH4_TK7xVyaIon8&callback=initMap"></script>
@endsection