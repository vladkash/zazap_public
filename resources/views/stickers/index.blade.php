@extends("layouts.admin")

@section("content")
    <div class="content content_active">
        <div class="wrap">
            <section class="main">
                <h2>управление стикерами</h2>
                <div class="this_yii">
                    <div class="one_yi">
                        <a href="{{route('addsticker')}}" class="btn btn-primary">Добавить стикер</a>
                    </div>
                </div>
                <div class="pagination_top">
                    <h3 href="" class="tag_strelka"><a href="{{route('stickers')}}">Управление стикерами</a></h3>
                </div>

                <div class="sticker_main">
                    @foreach($stickers as $sticker)
                        <div class="sticker_item">
                            <div class="this_sticker">
                                <img src="{{url('/storage/stickers/'.$sticker->sticker)}}" alt="">
                                <div class="stick_desc">
                                    <h3>Стикер {{$sticker->id}}</h3>
                                    <p>Название: {{$sticker->name}}</p>
                                </div>
                            </div>
                            <div class="colvo">
                                <div class="colvo_one">
                                    <p>Цена</p>
                                    <h4>{{$sticker->price}}</h4>
                                </div>
                                <div class="colvo_two">
                                    <p>Использован</p>
                                    <h4>{{$sticker->using}} раз</h4>
                                </div>
                                <div class="colvo_three">
                                    <a href="{{route('editsticker',['id'=>$sticker->id])}}"><img src="{{url('/assets/img/plus.jpg')}}" alt=""></a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>

                {{$stickers->links('layouts.pagination')}}

            </section>
        </div>
    </div>
@endsection