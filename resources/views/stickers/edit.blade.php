@extends("layouts.admin")

@section("content")
    <style>
        .cartochka_maps p {
            margin-top: 20px;
            margin-bottom: 10px;
            font-size: 16px;
        }
    </style>
    <div class="content content_active">
        <div class="wrap">
            <section class="main">
                <h2>управление стикерами</h2>
                <div class="this_yii">
                    <div class="one_yi">
                        <a href="{{route('addsticker')}}" class="btn btn-primary">Добавить стикер</a>
                    </div>
                </div>
                <div class="pagination_top">
                    <h3 href="" class="tag_strelka"><a href="{{route('stickers')}}">Управление стикерами</a></h3>
                    <h3 class="tag_strelka"><a href="{{route('editsticker',['id'=>$sticker->id])}}">Редактировать стикер</a> </h3>
                </div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div class="col-md-10">
                    <div class="cartochka_maps">
                        <form action="{{route('editsticker_p',['id'=>$sticker->id])}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <p>Название</p>
                            <input name="name" type="text" class="form-control" value="{{$sticker->name}}">
                            <p>Цена</p>
                            <input name="price" type="text" class="form-control" value="{{$sticker->price}}">
                            <p>Стикер</p>
                            <img src="{{url('storage/stickers/'.$sticker->sticker)}}" style="height: 100px;width: auto">
                            <input type="file" name="sticker" class="form-control-file">
                            <div class="btn_cart_maps">
                                <div class="col-md-6">
                                    <a href="{{route('stickerdelete',['id'=>$sticker->id])}}" class="btn btn-primary btn_delete btn-block">Удалить стикер</a>
                                </div>
                                <div class="col-md-6">
                                    <input type="submit" id="map-button" class="btn btn-primary btn-lg btn-block" value="Изменить стикер">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection