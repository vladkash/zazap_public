@extends("layouts.admin")

@section("content")
    <div class="content content_active">
        <div class="wrap">
            <section class="main">
                <h2>Продажи</h2>

                <h3 class="tag_strelka"><a href="{{route('sales')}}">Продажи</a></h3>

                <div class="filter_spisok">
                    <div class="user_spisok">

                    </div>
                    <a class="btn btn-primary filter_link" data-toggle="collapse" href="#multiCollapseExample1" role="button" aria-expanded="false" aria-controls="multiCollapseExample1"><img src="{{asset('/assets/img/filter.png')}}" alt="">Фильтр</a>
                </div>

                <div class="collapse multi-collapse" id="multiCollapseExample1">
                    <div class="card card-body">
                        <p class="close_filter_s">
                            Фильтр-поиск
                        </p>
                        <hr>

                        <form action="{{route('search_sale')}}" class="form_spisok" method="post">
                            @csrf
                            <div class="row">
                                <div class="col-lg-3">
                                    <label for="basic-url">Поиск</label>
                                    <div class="input-group mb-3">
                                        <input type="text" name="search_phrase" class="form-control" id="basic-url" aria-describedby="basic-addon3">
                                    </div>
                                    <div class="input-group mb-3">
                                        <div class="this_yii" style="width: 100%; margin-top: 0">
                                            <div class="one_yi" style="width: 100%">
                                                <input type="submit" class="btn btn-primary" value="Поиск" style="width: 100%">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <label for="basic-url">ID</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">ОТ</span>
                                        </div>
                                        <input type="text" name="id_from" class="form-control" aria-label="Amount (to the nearest dollar)">
                                    </div>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">ДО</span>
                                        </div>
                                        <input type="text" name="id_before" class="form-control" aria-label="Amount (to the nearest dollar)">
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <label for="basic-url">Дата транзакции</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">ОТ</span>
                                        </div>
                                        <input type="text" name="saledate_from" class="form-control date_pick"  aria-label="Amount (to the nearest dollar)">
                                    </div>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">ДО</span>
                                        </div>
                                        <input type="text" name="saledate_before" class="form-control date_pick" aria-label="Amount (to the nearest dollar)">
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <label for="basic-url">Сумма оплаты</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">ОТ</span>
                                        </div>
                                        <input type="text" name="amount_from" class="form-control" aria-label="Amount (to the nearest dollar)">
                                    </div>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">ДО</span>
                                        </div>
                                        <input type="text" name="amount_before" class="form-control" aria-label="Amount (to the nearest dollar)">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <table class="table table-borderless table_all_spisok tables_seller">
                    <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Тип</th>
                        <th scope="col">Дата, Время</th>
                        <th scope="col">Способ оплаты</th>
                        <th scope="col">Сумма транз.</th>
                        <th scope="col">Статус</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($sales as $sale)
                    <tr class="column_table">
                        <th scope="row"><a href="#">{{$sale->id}}</a></th>
                        <td>{{$sale->type}}</td>
                        <td>{{date('d.m.Y H:m',strtotime($sale->created_at))}}</td>
                        <td>{{$sale->method}}</td>
                        <td>{{$sale->amount}}руб.</td>
                        <td>
                        @if($sale->status == 'Зачислено')
                            <span class="success_c">Зачислено</span>
                        @elseif($sale->status == 'Ожидание')
                                <span class="yellow_c">Ожидание</span>
                        @elseif($sale->status == 'Отменено')
                                <span class="warning_c">Отменено</span>
                        @endif
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
                {{$sales->links('layouts.pagination')}}
            </section>
        </div>
    </div>
@endsection