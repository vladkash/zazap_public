@extends("layouts.admin")

@section("head")
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="{{ asset('js/app.js') }}" defer></script>
    <link rel="stylesheet" href="{{asset('assets/css/chat.css')}}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
@endsection

@section("content")
    <div class="content content_active">
        <div class="wrap">
            <section class="main">
                <h2>вопросы от пользователей</h2>
                <div class="this_yii">
                    <div class="one_yi">
                        <form action="{{route('change_question_status')}}" method="post">
                            <div class="form-group">
                                <label for="exampleFormControlSelect1">Сменить статус вопроса на:</label>
                                @csrf
                                <input type="hidden" name="question_id" value="{{$question->id}}">
                                <select name="status" class="form-control" id="exampleFormControlSelect1">
                                    <option value="В обработке" selected>Выберите статус</option>
                                    <option value="Новый">Новый</option>
                                    <option value="В обработке">В обработке</option>
                                    <option value="Обработан">Обработан</option>
                                </select>
                            </div>
                            <input type="submit" class="btn btn-primary" value="Сменить статус">
                        </form>
                    </div>
                </div>
                <div class="pagination_top">
                    <h3 class="tag_strelka"><a href="{{route('questions')}}">Вопросы</a></h3>
                    <h3 class="tag_strelka"><a href="{{route('question',['id'=>$question->id])}}">Чат с пользователем {{$user->id}}</a></h3>
                </div>
                <div id="app">
                    <chat-component :user="{{$user}}" :question_id="{{$question->id}}" :startmessages="{{$messages}}"></chat-component>
                </div>
            </section>
        </div>
    </div>
@endsection

@section("jsdefault")
@endsection