@extends("layouts.admin")

@section("content")
    <div class="content content_active">
        <div class="wrap">
            <section class="main">
                <h2>вопросы от пользователей</h2>
                <h3 class="tag_strelka"><a href="{{route('questions')}}">Вопросы</a></h3>

                <table class="table table-borderless">
                    <thead>
                    <tr>
                        <th scope="col">№</th>
                        <th scope="col">Вопрос</th>
                        <th scope="col">User ID</th>
                        <th scope="col">Дата, время</th>
                        <th scope="col">Статус</th>
                        <td></td>
                        <td></td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($questions as $question)
                    <tr class="column_table">
                        <th scope="row">{{$question->id}}</th>
                        <td>{{$question->first_message}}</td>
                        <td>{{$question->user_id}}</td>
                        <td>{{date('d.m.Y H:i',strtotime($question->created_at))}}</td>
                        <td>
                            <span class="@if($question->status == 'В обработке') wait_c @elseif($question->status == 'Новый') warning_c @elseif($question->status == 'Обработан') success_c @endif" >{{$question->status}}</span>
                        </td>
                        <td class="answer_block">
                            <a href="{{route('question',['id'=>$question->id])}}">Ответить</a>
                        </td>
                        <td class="answer_close">
                            <a href="{{route('questiondelete',['id'=>$question->id])}}">X</a>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
                {{$questions->links('layouts.pagination')}}
            </section>
        </div>
    </div>
@endsection