<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="ru"> <!--<![endif]-->

<!--

  _             _        _____            _
 | |           (_)      |  __ \          (_)
 | |_ __ __  __ _ _ __  | |  | | ___  ___ _  __ _ _ __
 | | '__|\ \/ /| | '_ \ | |  | |/ _ \/ __| |/ _` | '_ \
 | | |    \  / | | | | || |__| |  __/\__ \ | (_| | | | |
 |_|_|     \/  |_|_| |_||_____/ \___||___/_|\__, |_| |_|
                                             __/ |
                                            |___/
    kenrix@mail.ru  /  www.irvin.pro
-->

<head>

    <meta charset="utf-8">

    <title>Zazap admin panel</title>
    <meta name="description" content="">

    <link rel="shortcut icon" href="{{asset('/assets/img/favicon/favicon.ico')}}" type="image/x-icon">
    <link rel="apple-touch-icon" href="{{asset('/assets/img/favicon/apple-touch-icon.png')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{asset('/assets/img/favicon/apple-touch-icon-72x72.png')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{asset('/assets/img/favicon/apple-touch-icon-114x114.png')}}">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="stylesheet" href="{{asset('/assets/libs/bootstrap/css/bootstrap.min.css')}}">

    <link rel="stylesheet" href="{{asset('/assets/css/cssmin.css')}}">

    <script src="{{asset('/assets/libs/modernizr/modernizr.js')}}"></script>
    @yield("head")
</head>

<body>

<div class="wrapper">
    <div class="menu menu_active">
        <a href="#" class="menu-btn"><img src="{{asset('/assets/img/burger.png')}}" alt=""></a>
        <nav class="menu-list">
            <li><a href="{{route('dashboard')}}">Dashboard</a></li>
            <li><a href="{{route('users')}}">Пользователи</a></li>
            <li><a href="{{route('sales')}}">Продажи</a></li>
            <li><a href="{{route('placemarks')}}">Метки</a></li>
            <li><a href="{{route('photos')}}">Фото</a></li>
            <li><a href="{{route('stickers')}}">Стикеры</a></li>
            <li><a href="{{route('questions')}}">Вопросы</a></li>
            <li><a href="#">Настройки</a></li>
        </nav>
    </div>
    @yield("content")
</div>

<div class="hidden"></div>

<!--[if lt IE 9]>
<script src={{asset('/assets/"libs/html5shiv/es5-shim.min.js')}}"></script>
<script src="{{asset('/assets/libs/html5shiv/html5shiv.min.js')}}"></script>
<script src="{{asset('/assets/libs/html5shiv/html5shiv-printshiv.min.js')}}"></script>
<script src="{{asset('/assets/libs/respond/respond.min.js')}}"></script>
<![endif]-->

<script src="{{asset('/assets/libs/jquery/jquery-3.2.1.min.js')}}"></script>
@section("jsdefault")
    <script src="{{asset('/assets/js/jquery.maskedinput.min.js')}}"></script>
    <script src="{{asset('/assets/libs/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('/assets/libs/plugins-scroll/plugins-scroll.js')}}"></script>
    <script src="{{asset('/assets/js/common.js')}}"></script>
@show
@yield("js")
</body>
</html>