@extends("layouts.admin")

@section("content")
    <div class="content content_active">
        <div class="wrap">
            <section class="main">
                <h2>Dashboard</h2>
                <nav class="dashbord_main">
                    <li>
                        <a href="#">
                            <h2><img src="{{asset('/assets/img/d_icon_1.png')}}" alt="">Все пользователи</h2>
                            <h3>{{$all_users}}</h3>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <h2><img src="{{asset('/assets/img/d_icon_2.png')}}" alt="">Новые пользователи за месяц</h2>
                            <h3>{{$month_users}}</h3>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <h2><img src="{{asset('/assets/img/d_icon_3.png')}}" alt="">Новые пользователи за неделю</h2>
                            <h3>{{$week_users}}</h3>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <h2><img src="{{asset('/assets/img/d_icon_4.png')}}" alt="">Все продажи</h2>
                            <h3>{{$all_sales}}</h3>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <h2><img src="{{asset('/assets/img/d_icon_4.png')}}" alt="">Продажи VIP</h2>
                            <h3>{{$vip_sales}}</h3>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <h2><img src="{{asset('/assets/img/d_icon_4.png')}}" alt="">Продажи Sticker</h2>
                            <h3>{{$sticker_sales}}</h3>
                        </a>
                    </li>
                </nav>
            </section>
        </div>
    </div>
@endsection