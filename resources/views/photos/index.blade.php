@extends("layouts.admin")

@section("content")
    <div class="content content_active">
        <div class="wrap">
            <section class="main">
                <h2>Фото</h2>
                <div class="this_yii">
                    <div class="one_yi">
                        <a href="{{route('photo_history')}}" class="btn btn-primary">История удаленных</a>
                    </div>
                </div>
                <div class="pagination_top">
                    <h3 class="tag_strelka"><a href="{{route('photos')}}">Фото</a></h3>
                    @if($route == 'photo_history')
                        <h3 class="tag_strelka"><a href="{{route('photo_history')}}">История удаленных</a></h3>
                    @endif
                </div>
                <table class="table table-borderless table_all_spisok tables_seller">
                    <thead>
                    <tr>
                        <th scope="col">Фотография</th>
                        <th scope="col">Действия</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($photos as $photo)
                        <tr class="column_table">
                            <td style="padding: 10px">
                                <img style="width: 300px; height: 200px" src="{{url('/storage/avatars/'.$photo->name_photo)}}" alt="">
                            </td>
                            <td style="padding: 10px">
                                <div class="row">
                                    <a class="btn btn-primary mt-5 mb-5" href="{{route('photo_accept',['id'=>$photo->id])}}">Одобрить</a>
                                </div>
                                <div class="row">
                                    <a class="btn btn-danger" href="{{route('photo_reject',['id'=>$photo->id])}}">Отклонить</a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{$photos->links('layouts.pagination')}}
            </section>
        </div>
    </div>
@endsection