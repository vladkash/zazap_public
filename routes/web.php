<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('dashboard');
});


Auth::routes();

Route::get('/register',function(){
    return redirect('login');
})->name('register');

Route::get('/dashboard', 'HomeController@getDashboard')->name('dashboard');

Route::get('/users', 'HomeController@getUsers')->name('users');
Route::get('/user/{id}', 'HomeController@getUser')->name('user');
Route::get('/adduser', 'HomeController@getAddUser')->name('adduser');
Route::post('/search_user', 'SearchController@postSearchUser')->name('search_user');

Route::get('/userdelete/{id}', 'UserController@userDelete')->name('userdelete');
Route::post('/adduser_p', 'UserController@addUser')->name('adduser_p');
Route::post('/usersave/{id}', 'UserController@saveUser')->name('usersave');

Route::get('/placemarks', 'HomeController@getPlacemarks')->name('placemarks');
Route::get('/placemark/{id}', 'HomeController@getPlacemark')->name('placemark');
Route::get('/addplacemark', 'HomeController@getAddPlacemark')->name('addplacemark');

Route::get('/placemarkdelete/{id}', 'PlacemarkController@placemarkDelete')->name('placemarkdelete');
Route::get('/placemarkimagedelete/{id}', 'PlacemarkController@placemarkImageDelete')->name('placemarkimagedelete');
Route::post('/addplacemark_p', 'PlacemarkController@addPlacemark')->name('addplacemark_p');
Route::post('/placemarksave/{id}', 'PlacemarkController@savePlacemark')->name('placemarksave');

Route::get('/sales', 'HomeController@getSales')->name('sales');
Route::post('/search_sale', 'SearchController@postSearchSale')->name('search_sale');

Route::get('/photos', 'HomeController@getPhotos')->name('photos');
Route::get('/photos/history', 'HomeController@getPhotosHistory')->name('photo_history');

Route::get('/photo/accept/{id}', 'PhotoController@acceptPhoto')->name('photo_accept');
Route::get('/photo/reject/{id}', 'PhotoController@rejectPhoto')->name('photo_reject');

Route::get('/questions', 'HomeController@getQuestions')->name('questions');
Route::get('/question/{id}', 'HomeController@getQuestion')->name('question');

Route::get('/questiondelete/{id}', 'QuestionController@questionDelete')->name('questiondelete');
Route::post('/sendmessage', 'QuestionController@sendMessage')->name('sendmessage');
Route::post('/changequestionstatus', 'QuestionController@changeStatus')->name('change_question_status');

Route::get('/stickers', 'HomeController@getStickers')->name('stickers');
Route::get('/addsticker', 'HomeController@getAddSticker')->name('addsticker');
Route::get('/editsticker/{id}', 'HomeController@getEditSticker')->name('editsticker');

Route::post('/addsticker_p', 'StickerController@addSticker')->name('addsticker_p');
Route::get('/stickerdelete/{id}', 'StickerController@stickerDelete')->name('stickerdelete');
Route::post('/editsticker_p/{id}', 'StickerController@editSticker')->name('editsticker_p');

