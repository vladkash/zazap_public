<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//auth methods
Route::post('login', 'API\PassportController@login');
Route::post('register', 'API\PassportController@register');
Route::post('check-name', 'API\PassportController@checkName');
Route::post('check-email', 'API\PassportController@checkEmail');
Route::post('send-message', 'API\PassportController@sendMessage');
Route::post('check-phone', 'API\PassportController@checkPhone');
Route::post('get-guest-coordinates', 'API\CoordinatesController@getGuestCoordinates');
Route::post('get-markers', 'API\CoordinatesController@getMarkers');

Route::group(['middleware' => 'auth:api'], function(){
    //coordinates methods
    Route::post('get-coordinates', 'API\CoordinatesController@getCoordinates');
    //photos methods
    Route::post('add-photo', 'API\PhotoController@addPhoto');
    Route::post('delete-photo', 'API\PhotoController@deletePhoto');
    Route::post('change-avatar', 'API\PhotoController@changeAvatar');
    Route::post('report', 'API\PhotoController@report');
    //messages methods
    Route::post('get-chats', 'API\ChatController@getChats');
    Route::post('delete-chat', 'API\ChatController@deleteChat');
    Route::post('get-messages', 'API\MessageController@getMessages');
    Route::post('add-message', 'API\MessageController@addMessage');
    Route::post('delete-message', 'API\MessageController@deleteMessage');
    Route::post('check-new-messages', 'API\MessageController@checkNewMessages');
    //questions methods
    Route::post('get-questions', 'API\QuestionController@getQuestions');
    Route::post('get-question-messages', 'API\QuestionController@getQuestionMessages');
    Route::post('add-question-message', 'API\QuestionController@addQuestionMessage');
    //users methods
    Route::post('get-user-info', 'API\UserController@getUser');
    Route::post('update-profile', 'API\UserController@updateProfile');
    Route::post('get-current-user', 'API\UserController@getCurrentUser');
    Route::post('heartbeat', 'API\UserController@heartbeat');
    //blacklist methods
    Route::post('get-blacklist', 'API\BlacklistController@getBlacklist');
    Route::post('block-user', 'API\BlacklistController@blockUser');
    Route::post('unblock-user', 'API\BlacklistController@unblockUser');
    //filters methods
    Route::post('filter-users', 'API\FilterController@filterUsers');
    //subscribe methods
    Route::post('get-subscribes', 'API\SubscribeController@getSubscribes');
    Route::post('subscribe', 'API\SubscribeController@subscribe');
    Route::post('unsubscribe', 'API\SubscribeController@unsubscribe');
    //stickers methods
    Route::post('get-stickers', 'API\StickerController@getStickers');
    Route::post('add-sticker-message', 'API\StickerController@addStickerMessage');
    //vip methods
    Route::post('buy-vip', 'API\VipController@buyVip');
    //compliments methods
    Route::post('get-compliments', 'API\ComplimentController@getCompliments');
    Route::post('make-compliment', 'API\ComplimentController@makeCompliment');
    Route::post('delete-compliment', 'API\ComplimentController@deleteCompliment');
    //likes methods
    Route::post('like-photo', 'API\LikeController@likePhoto');
    Route::post('delete-like', 'API\LikeController@deleteLike');
    //notifications methods
    Route::post('get-notifications', 'API\NotificationController@getNotifications');
    Route::post('get-notifications-details', 'API\NotificationController@getNotificationsDetails');
});