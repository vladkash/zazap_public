<?php
use App\Question;
/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel('question.{id}', function ($user, $id) {
    if($user->is_admin){
        return true;
    }
    else {
        $question = Question::find($id);
        if ($question->user_id == $user->id) {
            return true;
        }else{
            return false;
        }
    }
});
