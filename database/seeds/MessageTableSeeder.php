<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Chat;
class MessageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $id = (int)User::max('id');
        $chat = Chat::create([
            'user1'=>$id -2,
            'user2'=>$id -1
        ]);
        $chat->messages()->create([
            'sender_id'=>$id -1,
            'recipient_id'=>$id - 2,
            'text'=>'Hello, user!'
        ]);
        $chat->messages()->create([
            'sender_id'=>$id-2,
            'recipient_id'=>$id-1,
            'text'=>'Hello you too, user '
        ]);
        $chat->messages()->create([
            'sender_id'=>$id -1,
            'recipient_id'=>$id - 2,
            'text'=>'How are you?'
        ]);
        $chat->messages()->create([
            'sender_id'=>$id-2,
            'recipient_id'=>$id-1,
            'text'=>'So so, How are you? '
        ]);
        $chat->messages()->create([
            'sender_id'=>$id -1,
            'recipient_id'=>$id - 2,
            'text'=>'I am fine, thanks. Bye!'
        ]);
        $chat->messages()->create([
            'sender_id'=>$id-2,
            'recipient_id'=>$id-1,
            'text'=>'bye!'
        ]);
    }
}
