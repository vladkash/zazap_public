<?php

use Illuminate\Database\Seeder;
use App\Sale;
use App\User;

class SalesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::where('is_admin',0)->get();
        foreach ($users as $user) {
            $user->sales()->create([
                'type'=>'VIP',
                'method'=>'QIWI',
                'amount'=>'1000',
                'status'=>'Зачислено'
            ]);
            $user->sales()->create([
                'type'=>'STICKER ГР.1',
                'method'=>'QIWI',
                'amount'=>'1000',
                'status'=>'Отменено'
            ]);
            $user->sales()->create([
                'type'=>'STICKER ГР.2',
                'method'=>'QIWI',
                'amount'=>'1000',
                'status'=>'Ожидание'
            ]);
        }

    }
}
