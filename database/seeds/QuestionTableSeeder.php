<?php

use Illuminate\Database\Seeder;

use App\User;


class QuestionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::where('is_admin', 0)->first();
        $question = $user->questions()->create([
            'status'=>'Новый'
        ]);
        $question->messages()->create([
            'is_admin'=>0,
            'readed'=>0,
            'message'=>'Hello, how can I buy VIP status?'
        ]);
    }
}
