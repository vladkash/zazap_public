<?php

use Illuminate\Database\Seeder;
use App\Placemark;

class PlacemarkTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Placemark::create([
            'name'=>'Кофейня Coffee Bean',
            'status'=>'Активна',
            'latitude'=>'55.745126',
            'longitude'=>'37.627295',
            'image'=>'placemark1.png'
        ]);
        Placemark::create([
            'name'=>'Кофейня ДаблБи',
            'status'=>'Активна',
            'latitude'=>'55.761893',
            'longitude'=>'37.635749',
            'image'=>'placemark2.png'
        ]);
        Placemark::create([
            'name'=>'Кофейня Starbucks',
            'status'=>'Активна',
            'latitude'=>'55.757253',
            'longitude'=>'37.616441',
            'image'=>'placemark3.png'
        ]);
        Placemark::create([
            'name'=>'Кофейня West 4',
            'status'=>'Активна',
            'latitude'=>'55.743577',
            'longitude'=>'37.601936',
            'image'=>'placemark4.png'
        ]);
    }
}
