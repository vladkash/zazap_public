<?php

use Illuminate\Database\Seeder;
use App\Photo;
use App\User;
class PhotoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::where('is_admin',0)->get();
        foreach ($users as $user) {
            $photo_id = Photo::max('id') + 1;
            $user->photo()->create([
                'name_photo'=>'user'.$photo_id.'avatar'.'.jpeg',
                'is_avatar'=> 0,
                'moderated'=> 0
            ]);
        }
    }
}
