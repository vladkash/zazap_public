$('.menu-btn').on('click', function(e) {
  e.preventDefault();
  $('.menu').toggleClass('menu_active');
  $('.content').toggleClass('content_active');
});


if (screen.width < 960) {

	$('.menu').removeClass('menu_active');
	$('.content').removeClass('content_active');
	
}

$(document).ready(function () {
    $('#show_users_list').click(function () {
        $('#users_map').css({"display":"none"});
        $('#users_list').css({"display":"block"});
    });
    $('#show_users_map').click(function () {
        $('#users_list').css({"display":"none"});
        $('#users_map').css({"display":"block"});
    });
    $('.date_pick').mask('99.99.9999');
    $('.phone_pick').mask('+7(999) 999-99-99');
});